google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);

function extractTableData(chartType, statType) {
    var data = Array.prototype.map.call(document.querySelectorAll('#chart_data tr'), function(tr){
      return Array.prototype.map.call(tr.querySelectorAll('td,th'), function(td){
        return td.innerHTML;
        });
      });

    console.log(data)

    var parseFn = null
    if(statType == "FLOAT") {
        parseFn = parseFloat
    } else {
        parseFn = parseInt
    }

    for (let row of data.slice(1)) {
        row[0] = new Date(row[0])
        row[1] = parseFn(row[1])
        if(chartType == "CANDLESTICK_CHART") {
            row[2] = parseFn(row[2])
            row[3] = parseFn(row[3])
            row[4] = parseFn(row[4])
        }
    }

    return google.visualization.arrayToDataTable(data, false)
}

function doChart(statName, chartType, chartData) {
    var chart = null
    if(chartType == "CANDLESTICK_CHART") {
        chart = new google.visualization.CandlestickChart(document.getElementById('stat_chart'));
    } else if(chartType == "LINE_CHART") {
        chart = new google.visualization.LineChart(document.getElementById('stat_chart'));
    }

    var options = null
    if(chartType == "CANDLESTICK_CHART") {
        options = {
              legend:'none'
            };
    } else {
        options = {
                  title: statName,
                  curveType: 'none',
                  legend: { position: 'bottom' }
                };
    }

    chart.draw(chartData, options);
}

function drawChart() {
    var statName = document.getElementById("stat_name").innerHTML
    var chartType = document.getElementById("chart_type").innerHTML
    var statType = document.getElementById("stat_type").innerHTML

    var chartData = extractTableData(chartType, statType)
    doChart(statName, chartType, chartData)
  }