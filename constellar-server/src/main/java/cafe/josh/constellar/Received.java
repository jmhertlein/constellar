package cafe.josh.constellar;

import java.time.ZonedDateTime;

public class Received<T> {
    private final T received;
    private final ZonedDateTime recvTs;
    private final String host;

    public Received(T received, String host, ZonedDateTime recvTs) {
        this.received = received;
        this.host = host;
        this.recvTs = recvTs;
    }

    public Received(T received, String host) {
        this(received, host, ZonedDateTime.now());
    }

    public T getReceived() {
        return received;
    }

    public ZonedDateTime getRcvTs() {
        return recvTs;
    }

    public String getHost() {
        return host;
    }
}
