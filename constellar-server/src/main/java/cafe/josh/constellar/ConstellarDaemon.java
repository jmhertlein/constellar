package cafe.josh.constellar;


import cafe.josh.constellar.cli.Role;
import cafe.josh.constellar.collector.Collector;
import cafe.josh.constellar.collectorui.CollectorWebServer;
import cafe.josh.constellar.emitter.Emitter;
import cafe.josh.constellar.message.util.GsonSingleton;
import cafe.josh.constellar.util.ConfigFinder;
import cafe.josh.constellar.util.opt.PathValueConverter;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import joptsimple.*;

import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Path;
import java.nio.file.Paths;

import static cafe.josh.constellar.cli.Role.COLLECTOR;
import static cafe.josh.constellar.cli.Role.EMITTER;

public class ConstellarDaemon {
    public static void main(String... args) throws IOException {
        OptionParser parser = new OptionParser();
        ArgumentAcceptingOptionSpec<Role> role = parser.accepts("role")
                .withRequiredArg()
                .ofType(Role.class)
                .describedAs("Role to run as.")
                .required();

        ArgumentAcceptingOptionSpec<Path> configDir = parser.accepts("config")
                .withRequiredArg()
                .withValuesConvertedBy(new PathValueConverter())
                .describedAs("Config dir to use. Default is /etc/constellar.d/");

        OptionSpec<Void> help = parser.accepts("help").forHelp();

        OptionSet options = parser.parse(args);

        if(options.has(help)) {
            parser.printHelpOn(System.out);
            return;
        }

        Role programRole = role.value(options);
        if(options.has(configDir)) {
            Path root = configDir.value(options);
            Log.info("Setting config root to: " + root.toAbsolutePath());
            ConfigFinder.setConfigRoot(root);
        }

        JsonParser json = new JsonParser();

        if(programRole == EMITTER) {
            Log.info("Running emitter.");
            JsonElement emitterConfig = json.parse(new InputStreamReader(ConfigFinder.openPreferredConfigSource("emitter.json")));
            JsonObject collectorDetails = emitterConfig.getAsJsonObject().getAsJsonObject("collector");
            JsonObject emitterDetails = emitterConfig.getAsJsonObject().getAsJsonObject("emitter");
            Emitter emitter = new Emitter(collectorDetails.get("host").getAsString(),
                    collectorDetails.get("port").getAsInt(),
                    emitterDetails.get("port").getAsInt());
            Runtime.getRuntime().addShutdownHook(new Thread() {
                @Override
                public void run() {
                }
            });

            try {
                emitter.run();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } else if(programRole == COLLECTOR) {
            Log.info("Running collector.");
            JsonElement collectorConfig = json.parse(new InputStreamReader(ConfigFinder.openPreferredConfigSource("collector.json")));
            JsonObject dbConfig = collectorConfig.getAsJsonObject().getAsJsonObject("db");
            Collector collector = new Collector(collectorConfig.getAsJsonObject().getAsJsonObject("collector").get("port").getAsInt(),
                    dbConfig.get("host").getAsString(),
                    dbConfig.get("port").getAsInt(),
                    dbConfig.get("name").getAsString(),
                    dbConfig.get("user").getAsString());
            CollectorWebServer web = new CollectorWebServer(collector, dbConfig.get("host").getAsString(),
                    dbConfig.get("name").getAsString(),
                    dbConfig.get("user").getAsString());
            Runtime.getRuntime().addShutdownHook(new Thread() {

                @Override
                public void run() {
                    try {
                        collector.stop();
                        web.stop();
                    } catch (Exception e) {
                        Log.warning("Interrupted while waiting for collector to stop.");
                    }
                }
            });
            web.start();
            try {
                collector.run();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
