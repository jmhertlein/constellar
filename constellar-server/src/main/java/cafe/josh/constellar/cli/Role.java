package cafe.josh.constellar.cli;

public enum Role {
    COLLECTOR,
    EMITTER;
}
