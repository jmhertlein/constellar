package cafe.josh.constellar.util;

import java.util.Enumeration;

public class Enumerations {
    private Enumerations() {}

    public static <T> boolean in(T o, Enumeration<T> enumer) {
        while(enumer.hasMoreElements()) {
            if(enumer.nextElement().equals(o)) {
                return true;
            }
        }

        return false;
    }

    public static boolean any(Enumeration<?> e) {
        while(e.hasMoreElements()) {
            if(e.nextElement() != null) {
                return true;
            }
        }
        return false;
    }
}
