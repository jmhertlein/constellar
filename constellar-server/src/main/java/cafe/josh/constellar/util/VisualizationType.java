package cafe.josh.constellar.util;

public enum VisualizationType {
    CANDLESTICK_CHART,
    LINE_CHART,
    BAR_CHART;

    public static VisualizationType match(String s) {
        return VisualizationType.valueOf(s.toUpperCase());
    }
}
