package cafe.josh.constellar.util;

public enum StatType {
    FLOAT,
    INT;

    public static StatType match(String s) {
        return StatType.valueOf(s.toUpperCase());
    }
}
