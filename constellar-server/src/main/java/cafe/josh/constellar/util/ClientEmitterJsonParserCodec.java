package cafe.josh.constellar.util;

import cafe.josh.constellar.Log;
import cafe.josh.constellar.message.clientemitter.ClientEmitterMessage;
import com.google.gson.JsonParser;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageCodec;

import java.util.List;

public class ClientEmitterJsonParserCodec extends ByteToMessageCodec<ClientEmitterMessage> {
    private final JsonParser parser;

    public ClientEmitterJsonParserCodec() {
        this.parser = new JsonParser();
    }

    @Override
    protected void encode(ChannelHandlerContext channelHandlerContext, ClientEmitterMessage msg, ByteBuf byteBuf) throws Exception {
        String rawMsg = msg.toJson().toString();
        byteBuf.writeBytes(rawMsg.getBytes("utf-8"));
        Log.debug("CODEC: coded json msg: " + rawMsg);
    }

    @Override
    protected void decode(ChannelHandlerContext channelHandlerContext, ByteBuf byteBuf, List<Object> list) throws Exception {
        byte[] bytes = new byte[byteBuf.readableBytes()];
        byteBuf.readBytes(bytes);
        String raw = new String(bytes, "utf-8");
        list.add(parser.parse(raw).getAsJsonObject());
        Log.debug("CODEC: Decoded json msg: raw");
    }
}
