package cafe.josh.constellar.util;

import cafe.josh.constellar.Log;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ConfigFinder {
    public static final Path
            SYS_CONFIG_ROOT_PATH = Paths.get("/etc", "constellar.d"),
            SYS_TRUST_PATH = SYS_CONFIG_ROOT_PATH.resolve("trust"),
            SYS_EMITTER_CERT_PATH = SYS_TRUST_PATH.resolve("emittercert.pem"),
            SYS_EMITTER_KEY_PATH = SYS_TRUST_PATH.resolve("emitterkey.pem"),
            SYS_COLLECTOR_CERT_PATH = SYS_TRUST_PATH.resolve("collectorcert.pem"),
            SYS_COLLECTOR_KEY_PATH = SYS_TRUST_PATH.resolve("collectorkey.pem"),
            SYS_CA_CERT_PATH = SYS_TRUST_PATH.resolve("cacert.pem");

    private static Path specifiedConfigRoot;

    static {
        specifiedConfigRoot = null;
    }

    public static void setConfigRoot(Path root) {
        specifiedConfigRoot = root;
    }

    public static InputStream openPreferredConfigSource(String configName) throws IOException {
        if(specifiedConfigRoot != null) {
            Path cur = specifiedConfigRoot.resolve(configName);
            if(Files.exists(cur)) {
                Log.info("Using config: " + cur.toAbsolutePath());
                return Files.newInputStream(cur);
            } else {
                throw new RuntimeException("Configuration file not found: " + cur.toString());
            }
        } else {
            Path etc = Paths.get("/etc", "constellar.d", configName);
            if(Files.exists(etc)) {
                Log.info("Using config: " + etc.toAbsolutePath());
                return Files.newInputStream(etc);
            }

            Log.info("Using " + configName + " from jar.");
            return ConfigFinder.class.getResourceAsStream("/config/" + configName);
        }
    }
}
