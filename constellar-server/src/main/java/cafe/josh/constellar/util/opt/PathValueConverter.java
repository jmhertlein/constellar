package cafe.josh.constellar.util.opt;

import joptsimple.ValueConverter;

import java.nio.file.Path;
import java.nio.file.Paths;

public class PathValueConverter implements ValueConverter<Path> {
    @Override
    public Path convert(String s) {
        return Paths.get(s);
    }

    @Override
    public Class<? extends Path> valueType() {
        return Path.class;
    }

    @Override
    public String valuePattern() {
        return null;
    }
}
