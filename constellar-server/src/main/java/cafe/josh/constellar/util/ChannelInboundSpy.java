package cafe.josh.constellar.util;

import cafe.josh.constellar.Log;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

import java.nio.charset.Charset;

public class ChannelInboundSpy extends ChannelInboundHandlerAdapter {

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        ByteBuf b = (ByteBuf) msg;
        Log.info("SPY: " + b.toString(Charset.forName("utf-8")));
        super.channelRead(ctx, msg);
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        Log.info("channel inactive");
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        Log.info("channel active");
    }

    @Override
    public void channelRegistered(ChannelHandlerContext ctx) throws Exception {
        Log.info("channel registered");
    }

    @Override
    public void channelUnregistered(ChannelHandlerContext ctx) throws Exception {
        Log.info("channel unregistered");
    }
}
