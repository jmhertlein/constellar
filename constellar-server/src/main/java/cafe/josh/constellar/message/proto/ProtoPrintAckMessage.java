package cafe.josh.constellar.message.proto;

import cafe.josh.constellar.message.emittercollector.EmitterCollectorMessageType;
import cafe.josh.constellar.message.gen.ProtoMessage;

import java.util.UUID;

@ProtoMessage(ecType = EmitterCollectorMessageType.PRINT_ACK)
public class ProtoPrintAckMessage {
    UUID ackedId;
}
