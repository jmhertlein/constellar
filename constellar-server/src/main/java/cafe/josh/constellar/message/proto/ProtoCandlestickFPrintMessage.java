package cafe.josh.constellar.message.proto;

import cafe.josh.constellar.message.clientemitter.ClientEmitterMessageType;
import cafe.josh.constellar.message.emittercollector.EmitterCollectorMessageType;
import cafe.josh.constellar.message.gen.ProtoMessage;

import java.time.ZonedDateTime;

@ProtoMessage(ceType=ClientEmitterMessageType.CANDLESTICK_F_PRINT,
ecType = EmitterCollectorMessageType.RELAYED_CANDLESTICK_F_PRINT,
isRelayed = true)
public class ProtoCandlestickFPrintMessage {
    ZonedDateTime mintTs, openTs, closeTs;
    String statName;
    double open, high, low, close;
}
