package cafe.josh.constellar.message.proto;

import cafe.josh.constellar.message.clientemitter.ClientEmitterMessageType;
import cafe.josh.constellar.message.emittercollector.EmitterCollectorMessageType;
import cafe.josh.constellar.message.gen.ProtoMessage;

import java.time.ZonedDateTime;

@ProtoMessage(ceType=ClientEmitterMessageType.CANDLESTICK_I_PRINT,
ecType = EmitterCollectorMessageType.RELAYED_CANDLESTICK_I_PRINT,
isRelayed = true)
public class ProtoCandlestickIPrintMessage {
    ZonedDateTime mintTs, openTs, closeTs;
    String statName;
    long open, high, low, close;
}
