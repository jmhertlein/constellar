package cafe.josh.constellar.message.proto;

import cafe.josh.constellar.message.emittercollector.EmitterCollectorMessageType;
import cafe.josh.constellar.message.gen.ProtoMessage;

import java.time.ZonedDateTime;
import java.util.List;

@ProtoMessage(ecType = EmitterCollectorMessageType.EMITTER_HEARTBEAT)
public class ProtoEmitterHeartbeatMessage {
    ZonedDateTime mintTs;
    List<String> connectedServices;
}
