package cafe.josh.constellar.message.proto;

import cafe.josh.constellar.message.clientemitter.ClientEmitterMessageType;
import cafe.josh.constellar.message.emittercollector.EmitterCollectorMessageType;
import cafe.josh.constellar.message.gen.ProtoMessage;

import java.time.ZonedDateTime;

@ProtoMessage(ceType = ClientEmitterMessageType.STATI_PRINT,
        ecType = EmitterCollectorMessageType.RELAYED_STATI_PRINT,
        isRelayed = true)
public class ProtoStatIPrintMessage {
    ZonedDateTime mintTs;
    String statName;
    long value;
}
