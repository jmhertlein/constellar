package cafe.josh.constellar.message.proto;

import cafe.josh.constellar.message.clientemitter.ClientEmitterMessageType;
import cafe.josh.constellar.message.emittercollector.EmitterCollectorMessageType;
import cafe.josh.constellar.message.gen.ProtoMessage;

import java.time.ZonedDateTime;

@ProtoMessage(ceType = ClientEmitterMessageType.STATF_PRINT,
ecType = EmitterCollectorMessageType.RELAYED_STATF_PRINT,
isRelayed = true)
public class ProtoStatFPrintMessage {
    ZonedDateTime mintTs;
    String statName;
    double value;
}
