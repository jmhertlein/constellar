package cafe.josh.constellar.message.proto;

import cafe.josh.constellar.message.clientemitter.ClientEmitterMessageType;
import cafe.josh.constellar.message.emittercollector.EmitterCollectorMessageType;
import cafe.josh.constellar.message.gen.ProtoMessage;
import cafe.josh.constellar.util.StatType;
import cafe.josh.constellar.util.VisualizationType;

@ProtoMessage(ceType = ClientEmitterMessageType.STAT_ANNOUNCEMENT,
ecType = EmitterCollectorMessageType.RELAYED_STAT_ANNOUNCEMENT,
isRelayed = true)
public class ProtoStatAnnouncementMessage {
    String statName;
    String prettyName;
    StatType statType;
    VisualizationType vizType;
    boolean hostSpecific;
}
