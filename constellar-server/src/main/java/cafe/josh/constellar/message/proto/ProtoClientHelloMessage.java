package cafe.josh.constellar.message.proto;

import cafe.josh.constellar.message.clientemitter.ClientEmitterMessageType;
import cafe.josh.constellar.message.gen.ProtoMessage;

@ProtoMessage(ceType = ClientEmitterMessageType.CLIENT_HELLO)
public class ProtoClientHelloMessage {
    String serviceName;
}
