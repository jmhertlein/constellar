package cafe.josh.constellar.message.proto;

import cafe.josh.constellar.message.emittercollector.EmitterCollectorMessageType;
import cafe.josh.constellar.message.gen.ProtoMessage;
import cafe.josh.constellar.message.util.MacAddress;

@ProtoMessage(ecType = EmitterCollectorMessageType.EMITTER_HELLO)
public class ProtoEmitterHelloMessage {
    String hostname;
    MacAddress macAddress;
    int cpuCount;
    long memBytes;
}
