package cafe.josh.constellar.message.proto;

import cafe.josh.constellar.message.clientemitter.ClientEmitterMessageType;
import cafe.josh.constellar.message.clientemitter.LogLevel;
import cafe.josh.constellar.message.emittercollector.EmitterCollectorMessageType;
import cafe.josh.constellar.message.gen.ProtoMessage;
import cafe.josh.constellar.message.gen.RelayedOnly;

import java.time.ZonedDateTime;

@ProtoMessage(ceType=ClientEmitterMessageType.LOG_PRINT,
        ecType = EmitterCollectorMessageType.RELAYED_LOG_PRINT,
        isRelayed = true)
public class ProtoLogPrintMessage {
    String message;
    LogLevel level;
    @RelayedOnly String serviceName;
    ZonedDateTime mintTs;
}
