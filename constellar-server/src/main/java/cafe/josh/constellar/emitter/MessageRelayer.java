package cafe.josh.constellar.emitter;

import cafe.josh.constellar.Log;
import cafe.josh.constellar.collector.Collector;
import cafe.josh.constellar.message.emittercollector.EmitterCollectorMessage;
import cafe.josh.constellar.message.emittercollector.EmitterHelloMessage;
import cafe.josh.constellar.message.util.MacAddress;
import cafe.josh.constellar.util.ConfigFinder;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.ssl.ClientAuth;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;
import io.netty.handler.ssl.util.InsecureTrustManagerFactory;
import oshi.SystemInfo;
import oshi.hardware.HardwareAbstractionLayer;

import javax.net.ssl.SSLException;
import java.io.File;
import java.net.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.ZonedDateTime;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;

public class MessageRelayer implements Runnable {
    private final Thread thread;
    private final String collectorHost;
    private final int collectorPort;
    private final LinkedBlockingQueue<EmitterCollectorMessage> queue;
    private final AtomicBoolean stop;

    private int triesSinceLastConnect;
    private final Map<UUID, EmitterCollectorMessage> floatingMessages;
    private final Map<UUID, ZonedDateTime> lastAckCheck;
    private final ChannelWriteListener writeListener;

    public MessageRelayer(String collectorHost, int collectorPort) {
        this.thread = new Thread(this);
        this.collectorHost = collectorHost;
        this.collectorPort = collectorPort;
        this.queue = new LinkedBlockingQueue<>();
        this.stop = new AtomicBoolean(false);
        this.floatingMessages = new ConcurrentHashMap<>();
        this.lastAckCheck = new ConcurrentHashMap<>();
        this.writeListener = new ChannelWriteListener();

        triesSinceLastConnect = 0;
    }

    @Override
    public void run() {
        EventLoopGroup workerGroup = new NioEventLoopGroup();
        Bootstrap b = null;
        try {
            b = bootstrapClient(workerGroup);
        } catch (SSLException e) {
            Log.exception("Error setting up SSL while connecting to collector.", e);
            return;
        }

        try {
            ChannelFuture f = null;
            reconnect:
            while (!stop.get()) {
                try {
                    triesSinceLastConnect += 1;
                    if (triesSinceLastConnect > 1) {
                        Log.warning("Sleeping before connecting to collector due to previous failures.");
                        Thread.sleep(Math.min(triesSinceLastConnect, 10) * 1000);
                    }
                    Log.info("Connecting to collector...");
                    f = b.connect(collectorHost, collectorPort).await();

                    Channel channel;
                    if (f != null && f.isSuccess()) {
                        channel = f.channel();
                        Log.info("Connected to collector.");
                        ChannelFuture helloWrite = null;
                        try {
                            SystemInfo si = new SystemInfo();
                            HardwareAbstractionLayer hw = si.getHardware();
                            int cpuCount = hw.getProcessor().getLogicalProcessorCount();
                            long memBytes = hw.getMemory().getTotal();
                            MacAddress macAddress = MacAddress.getMacAddress();
                            helloWrite = channel.writeAndFlush(new EmitterHelloMessage(InetAddress.getLocalHost().getHostName(), macAddress, cpuCount, memBytes)).sync();
                            triesSinceLastConnect = 0;
                            Log.info("Sent hello to collector as: " + InetAddress.getLocalHost().getHostName());
                        } catch (UnknownHostException e) {
                            Log.exception("Could not determine own hostname to send to Collector. Terminating.", e);
                            System.exit(-1);
                            return;
                        } catch (SocketException | MacAddress.UnableToDetermineMacAddressException e) {
                            Log.exception("Could not determine own mac address to send to Collector. Terminating.", e);
                            System.exit(-1);
                        }

                        if (helloWrite == null || !helloWrite.isSuccess()) {
                            Log.exception("Could not send hello to collector.", helloWrite.cause());
                            channel.close();
                            continue;
                        }
                    } else {
                        Log.severe("Could not connect to collector.");
                        continue;
                    }

                    while (!stop.get()) {
                        if (!channel.isWritable()) {
                            Log.severe("Channel is not writable anymore. Reconnecting.");
                            channel.close();
                            continue reconnect;
                        }

                        EmitterCollectorMessage taken = queue.take();
                        if(taken.getMessageType().isAckable()) {
                            floatingMessages.put(taken.getId(), taken);
                            lastAckCheck.put(taken.getId(), ZonedDateTime.now());
                        }
                        Log.info("SEND: " + taken.getId());
                        ChannelFuture write = channel.writeAndFlush(taken);
                        write.addListener(writeListener);
                    }
                } catch (InterruptedException e) {
                    Log.warning("Interrupted while syncing channel connect.");
                }
            }

            //TODO: try to flush floating messages and rest of queue

            if (f != null && f.isSuccess()) {
                try {
                    f.channel().close().sync();
                } catch (InterruptedException e) {
                    Log.warning("Interrupted while syncing MessageRelayer channel close.");
                }
            }
        } finally {
            workerGroup.shutdownGracefully();
        }
    }

    private Bootstrap bootstrapClient(EventLoopGroup workerGroup) throws SSLException {
        SslContext sslCtx = SslContextBuilder.forClient()
                .clientAuth(ClientAuth.REQUIRE)
                .keyManager(ConfigFinder.SYS_EMITTER_CERT_PATH.toFile(), ConfigFinder.SYS_EMITTER_KEY_PATH.toFile())
                .trustManager(ConfigFinder.SYS_CA_CERT_PATH.toFile()).build();
        Bootstrap b = new Bootstrap();
        b.group(workerGroup);
        b.option(ChannelOption.CONNECT_TIMEOUT_MILLIS, 1000);
        b.channel(NioSocketChannel.class);
        b.option(ChannelOption.SO_KEEPALIVE, true);
        b.handler(new CollectorClientChannelInitializer(sslCtx, this));
        return b;
    }

    public void send(EmitterCollectorMessage m) {
        this.queue.add(m);
    }

    public Map<UUID, EmitterCollectorMessage> getFloatingMessages() {
        return floatingMessages;
    }

    public Map<UUID, ZonedDateTime> getLastAckCheck() {
        return lastAckCheck;
    }

    public void start() {
        this.thread.start();
        Log.info("Relayer thread started.");
    }

    public void interrupt() {
        this.thread.interrupt();
        Log.debug("Interrupted relayer thread.");
    }
}
