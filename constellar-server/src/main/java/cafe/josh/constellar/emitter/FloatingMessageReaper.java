package cafe.josh.constellar.emitter;

import cafe.josh.constellar.Log;
import cafe.josh.constellar.message.emittercollector.EmitterCollectorMessage;

import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

public class FloatingMessageReaper implements Runnable {
    private final MessageRelayer relayer;

    public FloatingMessageReaper(MessageRelayer relayer) {
        this.relayer = relayer;
    }

    @Override
    public void run() {
        Map<UUID, EmitterCollectorMessage> floatingMessages = relayer.getFloatingMessages();
        Map<UUID, ZonedDateTime> lastAckCheck = relayer.getLastAckCheck();

        Set<UUID> longFloating = new HashSet<>();
        for(UUID id : floatingMessages.keySet()) {
            ZonedDateTime lastCheck = lastAckCheck.get(id);
            if(lastCheck == null) {
                continue;
            }

            if(ChronoUnit.MINUTES.between(lastCheck, ZonedDateTime.now()) > 1) {
                longFloating.add(id);
            }
        }

        if(!longFloating.isEmpty()) {
            Log.warning(String.format("Found %s prints floating for > 1 minute. Resending.", longFloating.size()));
            for (UUID id : longFloating) {
                EmitterCollectorMessage remove = floatingMessages.remove(id);
                lastAckCheck.remove(id);
                relayer.send(remove);
            }
        }
    }
}
