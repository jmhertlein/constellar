package cafe.josh.constellar.emitter;

import cafe.josh.constellar.Log;
import cafe.josh.constellar.message.clientemitter.*;
import com.google.gson.JsonObject;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageDecoder;

import java.util.List;

public class JsonObjectToClientEmitterDecoder extends MessageToMessageDecoder<JsonObject> {
    @Override
    protected void decode(ChannelHandlerContext channelHandlerContext, JsonObject o, List<Object> list) throws Exception {
        ClientEmitterMessageType type = ClientEmitterMessageType.ofJson(o);
        Log.debug("Decoding JsonObject for " + type.name());
        switch(type) {
            case CLIENT_HELLO:
                list.add(new ClientHelloMessage(o));
                return;
            case LOG_PRINT:
                list.add(new LogPrintMessage(o));
                return;
            case STATF_PRINT:
                list.add(new StatFPrintMessage(o));
                return;
            case STATI_PRINT:
                list.add(new StatIPrintMessage(o));
                return;
            case CANDLESTICK_F_PRINT:
                list.add(new CandlestickFPrintMessage(o));
                return;
            case CANDLESTICK_I_PRINT:
                list.add(new CandlestickIPrintMessage(o));
                return;
            case STAT_ANNOUNCEMENT:
                list.add(new StatAnnouncementMessage(o));
                return;
            default:
                throw new UnsupportedOperationException("Unhandled enum value: " + type.name());
        }
    }
}