package cafe.josh.constellar.emitter;

import cafe.josh.constellar.util.ClientEmitterJsonParserCodec;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.json.JsonObjectDecoder;

public class EmitterChannelInitializer extends ChannelInitializer<SocketChannel> {
    private final MessageRelayer relayer;

    public EmitterChannelInitializer(MessageRelayer relayer) {
        this.relayer = relayer;
    }

    @Override
    protected void initChannel(SocketChannel socketChannel) throws Exception {
        ChannelPipeline p = socketChannel.pipeline();
        p.addLast(new JsonObjectDecoder());
        p.addLast(new ClientEmitterJsonParserCodec());
        p.addLast(new JsonObjectToClientEmitterDecoder());
        p.addLast(new EmitterClientHandler(relayer));
    }
}
