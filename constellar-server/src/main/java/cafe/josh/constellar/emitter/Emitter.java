package cafe.josh.constellar.emitter;

import cafe.josh.constellar.Log;
import cafe.josh.constellar.client.builtin.HostMonitor;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;

import java.io.IOException;
import java.util.concurrent.*;

public class Emitter {
    public static final int STANDARD_PORT = 42523;
    public static final int REAPER_DELAY_SECONDS = 10;
    public static final int HEARTBEAT_DELAY_SECONDS = 5;

    private final int port;

    private final ExecutorService pool;
    private final ScheduledExecutorService scheduledPool;

    private final MessageRelayer relayer;
    private final FloatingMessageReaper reaper;
    private final EmitterHeart heart;
    private Channel channel;

    public Emitter(String collectorHost, int collectorPort, int listenPort) {
        this.port = listenPort;
        pool = Executors.newCachedThreadPool();
        scheduledPool = Executors.newSingleThreadScheduledExecutor();

        relayer = new MessageRelayer(collectorHost, collectorPort);
        reaper = new FloatingMessageReaper(relayer);
        heart = new EmitterHeart(relayer);
    }

    public void run() throws IOException, InterruptedException {
        Log.debug("Emitter starting up.");
        scheduledPool.scheduleWithFixedDelay(reaper, REAPER_DELAY_SECONDS, REAPER_DELAY_SECONDS, TimeUnit.SECONDS);
        scheduledPool.scheduleWithFixedDelay(heart, HEARTBEAT_DELAY_SECONDS, HEARTBEAT_DELAY_SECONDS, TimeUnit.SECONDS);
        relayer.start();

        pool.submit(new HostMonitor());

        EventLoopGroup bossGroup = new NioEventLoopGroup();
        EventLoopGroup workerGroup = new NioEventLoopGroup();
        try {
            ServerBootstrap b = bootstrap(bossGroup, workerGroup);
            Log.info("Binding to port " + port);
            channel = b.bind(port).channel();
            channel.closeFuture().sync();
            Log.info("Port unbound.");
        } finally {
            workerGroup.shutdownGracefully();
            bossGroup.shutdownGracefully();
        }

    }

    private ServerBootstrap bootstrap(EventLoopGroup bossGroup, EventLoopGroup workerGroup) {
        ServerBootstrap b = new ServerBootstrap();
        b.group(bossGroup, workerGroup)
                .channel(NioServerSocketChannel.class)
                .childHandler(new EmitterChannelInitializer(relayer))
                .option(ChannelOption.SO_BACKLOG, 128)
                .childOption(ChannelOption.SO_KEEPALIVE, true);
        return b;
    }
}
