package cafe.josh.constellar.emitter;

import cafe.josh.constellar.Log;
import cafe.josh.constellar.message.emittercollector.EmitterHeartbeatMessage;

import java.time.ZonedDateTime;
import java.util.ArrayList;

public class EmitterHeart implements Runnable {
    private final MessageRelayer relayer;

    public EmitterHeart(MessageRelayer relayer) {
        this.relayer = relayer;
    }

    @Override
    public void run() {
        relayer.send(new EmitterHeartbeatMessage(ZonedDateTime.now(), new ArrayList<>()));
        Log.debug("[sent heartbeat]");
    }
}
