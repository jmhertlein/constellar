package cafe.josh.constellar.emitter;

import cafe.josh.constellar.Log;
import cafe.josh.constellar.message.clientemitter.*;
import cafe.josh.constellar.message.emittercollector.*;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

public class EmitterClientHandler extends ChannelInboundHandlerAdapter {
    private final MessageRelayer relayer;

    private String serviceName;

    public EmitterClientHandler(MessageRelayer relayer) {
        this.relayer = relayer;
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        ClientEmitterMessage m = (ClientEmitterMessage) msg;
        switch(m.getMessageType()) {
            case CLIENT_HELLO:
                serviceName = ((ClientHelloMessage) m).getServiceName();
                Log.info("HELLO: " + serviceName);
                break;
            case STAT_ANNOUNCEMENT:
                relayer.send(new RelayedStatAnnouncementMessage((StatAnnouncementMessage) m));
                break;
            case LOG_PRINT:
                relayer.send(new RelayedLogPrintMessage((LogPrintMessage) m, serviceName));
                break;
            case STATI_PRINT:
                relayer.send(new RelayedStatIPrintMessage((StatIPrintMessage) m));
                break;
            case STATF_PRINT:
                relayer.send(new RelayedStatFPrintMessage((StatFPrintMessage) m));
                break;
            case CANDLESTICK_F_PRINT:
                relayer.send(new RelayedCandlestickFPrintMessage((CandlestickFPrintMessage) m));
                break;
            case CANDLESTICK_I_PRINT:
                relayer.send(new RelayedCandlestickIPrintMessage((CandlestickIPrintMessage) m));
                break;
            default:
                throw new UnsupportedOperationException("Unhandled message type: " + m.getMessageType().name());
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        Log.exception("Exception caught.", cause);
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        Log.info("Client lost: " + serviceName);
    }
}
