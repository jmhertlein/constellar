package cafe.josh.constellar.emitter;

import cafe.josh.constellar.Log;
import cafe.josh.constellar.message.emittercollector.EmitterCollectorMessage;
import cafe.josh.constellar.message.emittercollector.PrintAckMessage;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;


public class CollectorClientHandler extends ChannelInboundHandlerAdapter {
    private final MessageRelayer relayer;

    public CollectorClientHandler(MessageRelayer relayer) {
        this.relayer = relayer;
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        EmitterCollectorMessage m = (EmitterCollectorMessage) msg;
        switch(m.getMessageType()) {
            case PRINT_ACK:
                PrintAckMessage ack = (PrintAckMessage) m;
                Log.debug("Server sent ACK: " + ack.getAckedId());
                relayer.getFloatingMessages().remove(ack.getAckedId());
                relayer.getLastAckCheck().remove(ack.getAckedId());
                break;
            default:
                Log.warning(String.format("Received improper message type for collector client: %s.", m.getMessageType().name()));
                break;

        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        Log.exception("Exception caught.", cause);
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        Log.info("Channel inactive.");
        check(ctx.channel());
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        Log.info("Channel active.");
    }

    @Override
    public void channelWritabilityChanged(ChannelHandlerContext ctx) throws Exception {
        check(ctx.channel());
    }

    private void check(Channel ch) {
        if(ch.isWritable()) {
            Log.warning("Something happened to cause us to check channel writability, but everything was OK.");
        } else {
            relayer.interrupt();
            Log.warning("Relayer connection to collector went unwritable. Interrupting.");
        }
    }
}
