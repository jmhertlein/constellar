package cafe.josh.constellar.emitter;

import cafe.josh.constellar.Log;
import cafe.josh.constellar.message.clientemitter.ClientEmitterMessageType;
import cafe.josh.constellar.message.clientemitter.ClientHelloMessage;
import cafe.josh.constellar.message.clientemitter.LogPrintMessage;
import cafe.josh.constellar.message.clientemitter.StatFPrintMessage;
import cafe.josh.constellar.message.clientemitter.StatIPrintMessage;
import cafe.josh.constellar.message.emittercollector.RelayedLogPrintMessage;
import cafe.josh.constellar.message.emittercollector.RelayedStatFPrintMessage;
import cafe.josh.constellar.message.emittercollector.RelayedStatIPrintMessage;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Socket;

public class
EmitterHandler implements Runnable {
    private final MessageRelayer relayer;
    private final Socket client;
    private final JsonParser json;

    public EmitterHandler(MessageRelayer relayer, Socket client) {
        this.relayer = relayer;
        this.client = client;
        this.json = new JsonParser();
    }

    @Override
    public void run() {
        System.out.println("Client handled: " + client.getInetAddress());
        String serviceName = null;
        try(InputStream is = client.getInputStream(); BufferedReader in = new BufferedReader(new InputStreamReader(is))) {
            while(true) {
                String raw = in.readLine();
                JsonObject rawJson = json.parse(raw).getAsJsonObject();
                ClientEmitterMessageType type = ClientEmitterMessageType.valueOf(rawJson.get("type").getAsString());
                switch(type) {
                    case CLIENT_HELLO:
                        serviceName = new ClientHelloMessage(rawJson).getServiceName();
                        Log.info("HELLO: " + serviceName);
                        break;
                    case LOG_PRINT:
                        relayer.send(new RelayedLogPrintMessage(new LogPrintMessage(rawJson), serviceName));
                        break;
                    case STATI_PRINT:
                        relayer.send(new RelayedStatIPrintMessage(new StatIPrintMessage(rawJson)));
                        break;
                    case STATF_PRINT:
                        relayer.send(new RelayedStatFPrintMessage(new StatFPrintMessage(rawJson)));
                        break;
                    default:
                        throw new UnsupportedOperationException("Unhandled message type: " + type.name());
                }
            }
        } catch(IOException e) {
            e.printStackTrace();
        }
    }
}
