package cafe.josh.constellar.emitter;

import cafe.josh.constellar.Log;
import io.netty.util.concurrent.Future;
import io.netty.util.concurrent.GenericFutureListener;

public class ChannelWriteListener implements GenericFutureListener<Future<Void>> {
    @Override
    public void operationComplete(Future<Void> future) throws Exception {
        if (future.isSuccess()) {
            // nothing
        } else {
            Throwable cause = future.cause();
            if(cause != null) {
                Log.exception("Write to collector failed.", cause);
            } else {
                Log.severe("Write to collector failed, and cause() was null.");
            }
        }
    }
}
