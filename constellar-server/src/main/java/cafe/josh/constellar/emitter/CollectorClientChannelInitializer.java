package cafe.josh.constellar.emitter;

import cafe.josh.constellar.collector.JsonObjectToCollectorEmitterDecoder;
import cafe.josh.constellar.util.EmitterCollectorJsonParserCodec;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.json.JsonObjectDecoder;
import io.netty.handler.ssl.SslContext;

public class CollectorClientChannelInitializer extends ChannelInitializer<SocketChannel> {
    private final SslContext sslCtx;
    private final MessageRelayer messageRelayer;

    public CollectorClientChannelInitializer(SslContext sslCtx, MessageRelayer messageRelayer) {
        this.sslCtx = sslCtx;
        this.messageRelayer = messageRelayer;
    }

    @Override
    protected void initChannel(SocketChannel socketChannel) throws Exception {
        ChannelPipeline p = socketChannel.pipeline();
        p.addLast(sslCtx.newHandler(socketChannel.alloc()));
        p.addLast(new JsonObjectDecoder());
        p.addLast(new EmitterCollectorJsonParserCodec());
        p.addLast(new JsonObjectToCollectorEmitterDecoder());
        p.addLast(new CollectorClientHandler(messageRelayer));
    }
}
