package cafe.josh.constellar.client.builtin;

import java.util.Map;

public class OSSnapshot {
    private final double cpuUseRatio;
    private final long usedMemory;
    private final Map<String, Double> diskUsage;
    private final long bytesSent, bytesReceived;

    public OSSnapshot(double cpuUseRatio, long usedMemory, Map<String, Double> diskUsage, long bytesSent, long bytesReceived) {
        this.cpuUseRatio = cpuUseRatio;
        this.usedMemory = usedMemory;
        this.diskUsage = diskUsage;
        this.bytesSent = bytesSent;
        this.bytesReceived = bytesReceived;
    }

    public long getUsedMemory() {
        return usedMemory;
    }

    public double getCpuUseRatio() {
        return cpuUseRatio;
    }

    public Map<String, Double> getDiskUsage() {
        return diskUsage;
    }

    public long getBytesSent() {
        return bytesSent;
    }

    public long getBytesReceived() {
        return bytesReceived;
    }
}
