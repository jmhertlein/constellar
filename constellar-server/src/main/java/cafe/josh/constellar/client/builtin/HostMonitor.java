package cafe.josh.constellar.client.builtin;

import cafe.josh.constellar.Log;
import cafe.josh.constellar.client.EmitterConnection;
import cafe.josh.constellar.emitter.Emitter;
import cafe.josh.constellar.util.StatType;
import cafe.josh.constellar.util.VisualizationType;
import oshi.SystemInfo;
import oshi.hardware.HardwareAbstractionLayer;
import oshi.hardware.NetworkIF;
import oshi.software.os.OSFileStore;
import oshi.software.os.OperatingSystem;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HostMonitor implements Runnable {
    private final Pattern DRIVE_NAME_PATTERN = Pattern.compile("^((/dev/(?<sdname>sd[a-z]+))|((?<dletter>[A-Z]+):\\\\))$");
    private final AtomicBoolean run;

    private final HardwareAbstractionLayer hal;
    private final OperatingSystem os;
    private final int POLLING_INTERVAL_SECONDS = 1, CANDLESTICK_INTERVAL_COUNT = 60;

    public HostMonitor() {
        this.run = new AtomicBoolean(true);
        SystemInfo si = new SystemInfo();
        hal = si.getHardware();
        os = si.getOperatingSystem();
    }

    @Override
    public void run() {
        try (EmitterConnection conn = new EmitterConnection("HostMonitor")) {
            conn.announceStat("cpu_use_ratio", "CPU Use Ratio", StatType.FLOAT, VisualizationType.CANDLESTICK_CHART);
            conn.announceStat("mem_bytes_used", "Bytes of Memory Used", StatType.INT, VisualizationType.CANDLESTICK_CHART);
            conn.announceStat("network_total_bytes_transferred", "Network Total Bytes Transferred", StatType.INT, VisualizationType.LINE_CHART);
            OHLC<Double> cpuCandle = new OHLC<>();
            OHLC<Long> memCandle = new OHLC<>();
            while (run.get()) {
                Map<String, Double> diskUsages = null;
                OSSnapshot first = null, last = null;
                for (int i = 0; i < CANDLESTICK_INTERVAL_COUNT; i++) {
                    OSSnapshot profile = profile();
                    if (first == null) {
                        first = profile;
                    }
                    last = profile;
                    cpuCandle.update(profile.getCpuUseRatio());
                    memCandle.update(profile.getUsedMemory());
                    diskUsages = profile.getDiskUsage();
                    try {
                        Thread.sleep(POLLING_INTERVAL_SECONDS * 1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                //System.out.println("CPU: " + cpuCandle);
                conn.candlef("cpu_use_ratio", cpuCandle.getOpenTs(), cpuCandle.getCloseTs(), cpuCandle.getOpen(), cpuCandle.getHigh(), cpuCandle.getLow(), cpuCandle.getClose());
                cpuCandle.cut();
                //System.out.println("MEM: " + memCandle);
                conn.candlei("mem_bytes_used", memCandle.getOpenTs(), memCandle.getCloseTs(), memCandle.getOpen(), memCandle.getHigh(), memCandle.getLow(), memCandle.getClose());
                memCandle.cut();
                //System.out.println("Disk Usages:");
                for (Map.Entry<String, Double> diskUsage : diskUsages.entrySet()) {
                    //System.out.println(diskUsage.getKey() + ": " + (diskUsage.getValue() * 100) + "%");
                    Optional<String> deviceName = cleanDriveName(diskUsage.getKey());
                    deviceName.ifPresent(name -> {
                        conn.announceStat("disk_usage_" + name, "Ratio of Disk Usage of " + name, StatType.FLOAT, VisualizationType.LINE_CHART);
                        conn.statf("disk_usage_" + name, diskUsage.getValue());
                    });
                }
                long diffSent = last.getBytesSent() - first.getBytesSent();
                long diffRecv = last.getBytesReceived() - first.getBytesReceived();
                //System.out.println("Bytes: " + (diffSent + diffRecv));
                conn.stati("network_total_bytes_transferred", diffRecv + diffSent);
            }
        } catch (IOException e) {
            Log.exception("Error in connection to emitter.", e);
        }
    }

    public OSSnapshot profile() {
        double systemCpuLoad = hal.getProcessor().getSystemCpuLoad();
        long usedMemory = hal.getMemory().getTotal() - hal.getMemory().getAvailable();
        Map<String, Double> diskUsages = new HashMap<>();
        for (OSFileStore osFileStore : os.getFileSystem().getFileStores()) {
            diskUsages.put(osFileStore.getName(), (osFileStore.getTotalSpace() - osFileStore.getUsableSpace()) / ((double) osFileStore.getTotalSpace()));
        }
        long bytesSent = 0, bytesReceived = 0;
        for (NetworkIF networkIF : hal.getNetworkIFs()) {
            bytesReceived += networkIF.getBytesRecv();
            bytesSent += networkIF.getBytesSent();
        }

        return new OSSnapshot(systemCpuLoad, usedMemory, diskUsages, bytesSent, bytesReceived);
    }

    public static void main(String... args) {
        HostMonitor m = new HostMonitor();
        m.run();
    }

    private Optional<String> cleanDriveName(String n) {
        Matcher matcher = DRIVE_NAME_PATTERN.matcher(n);
        if(matcher.matches()) {
            String sdName = matcher.group("sdname");
            String dLetter = matcher.group("dletter");
            if(sdName != null) {
                return Optional.of(sdName);
            } else if(dLetter != null) {
                return Optional.of(dLetter);
            } else {
                throw new RuntimeException("Regex matched but both sdname and dletter were null?");
            }
        } else {
            return Optional.empty();
        }
    }
}
