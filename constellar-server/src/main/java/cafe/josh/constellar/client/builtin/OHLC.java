package cafe.josh.constellar.client.builtin;

import java.time.ZonedDateTime;

public class OHLC<T extends Comparable<? super T>> {
    private T open, high, low, close;
    private ZonedDateTime openTs, closeTs;
    public OHLC() {

    }

    public T getOpen() {
        return open;
    }

    public T getHigh() {
        return high;
    }

    public T getLow() {
        return low;
    }

    public T getClose() {
        return close;
    }

    public ZonedDateTime getOpenTs() {
        return openTs;
    }

    public ZonedDateTime getCloseTs() {
        return closeTs;
    }

    public void update(T value) {
        if(open == null) {
            open = value;
            high = value;
            low = value;
            close = value;
            openTs = ZonedDateTime.now();
            closeTs = openTs;
        } else {
            if(value.compareTo(high) > 0) {
                high = value;
            }
            if(value.compareTo(low) < 0) {
                low = value;
            }
            close = value;
            closeTs = ZonedDateTime.now();
        }
    }

    public void cut() {
        open = close;
        high = open;
        low = open;
        openTs = ZonedDateTime.now();
        closeTs = openTs;
    }

    @Override
    public String toString() {
        return String.format("{%s, %s, %s, %s}", open, high, low, close);
    }
}
