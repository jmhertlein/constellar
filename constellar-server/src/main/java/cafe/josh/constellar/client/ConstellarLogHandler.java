package cafe.josh.constellar.client;

import cafe.josh.constellar.message.clientemitter.LogLevel;

import java.io.IOException;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

public class ConstellarLogHandler extends Handler {
    private EmitterConnection conn;
    public ConstellarLogHandler(String serviceName) throws IOException {
        conn = new EmitterConnection(serviceName);
    }

    @Override
    public void publish(LogRecord record) {
        conn.log(LogLevel.getFor(record.getLevel()), record.getMessage());
    }

    @Override
    public void flush() {
        conn.flush();
    }

    @Override
    public void close() throws SecurityException {
        try {
            conn.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
