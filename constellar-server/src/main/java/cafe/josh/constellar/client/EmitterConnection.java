package cafe.josh.constellar.client;

import cafe.josh.constellar.Log;
import cafe.josh.constellar.emitter.Emitter;
import cafe.josh.constellar.message.clientemitter.*;
import cafe.josh.constellar.util.StatType;
import cafe.josh.constellar.util.VisualizationType;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;

import java.io.IOException;
import java.net.InetAddress;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;

public class EmitterConnection implements AutoCloseable {
    private EventLoopGroup workerGroup;
    private final Channel channel;

    private final Set<String> announcedStats;

    public EmitterConnection(String serviceName) throws IOException {
        this(serviceName, Emitter.STANDARD_PORT);
    }

    public EmitterConnection(String serviceName, int port) throws IOException {
        this.announcedStats = new HashSet<>();
        workerGroup = new NioEventLoopGroup();
        Bootstrap b = bootstrapClient(workerGroup);
        ChannelFuture channelFuture = b.connect(InetAddress.getLocalHost(), port);
        try {
            channelFuture.await();
        } catch (InterruptedException e) {
            Log.warning("Interrupted while connecting to emitter.");
        }

        if(channelFuture.isSuccess()) {
            channel = channelFuture.channel();
            ChannelFuture helloFuture = channel.writeAndFlush(new ClientHelloMessage(serviceName));
            try {
                helloFuture.await();
            } catch (InterruptedException e) {
                Log.warning("Interrupted while awaiting hello send.");
            }
            if(!helloFuture.isSuccess()) {
                Log.exception("Error saying hello to emitter.", helloFuture.cause());
                throw new IOException("Error saying hello to emitter.", helloFuture.cause());
            }
        } else {
            throw new IOException("Error connecting to emitter on port " + port, channelFuture.cause());
        }
    }

    public void debug(String message) {
        log(LogLevel.DEBUG, message);
    }

    public void info(String message) {
        log(LogLevel.INFO, message);
    }

    public void warning(String message) {
        log(LogLevel.WARNING, message);
    }

    public void critical(String message) {
        log(LogLevel.CRITICAL, message);
    }

    public void announceStat(String statName, String prettyName, StatType type, VisualizationType vizType) {
        announceStat(statName, prettyName, type, vizType, true);
    }

    public void announceStat(String statName, String prettyName, StatType type, VisualizationType vizType, boolean hostSpecific) {
        if(!announcedStats.contains(statName)) {
            Log.info("Announcing: " + statName + " - " + type.name() + " - " + vizType.name());
            channel.writeAndFlush(new StatAnnouncementMessage(statName, prettyName, type, vizType, hostSpecific));
            announcedStats.add(statName);
        }
    }

    public void ensureAnnounced(String statName) {
        if(!announcedStats.contains(statName)) {
            throw new RuntimeException("Unannounced stat: " + statName);
        }
    }

    public void log(LogLevel level, String message) {
        channel.writeAndFlush(new LogPrintMessage(message, level, ZonedDateTime.now()));
    }

    public void statf(String statName, double value) {
        ensureAnnounced(statName);
        channel.writeAndFlush(new StatFPrintMessage(ZonedDateTime.now(), statName, value));
    }

    public void stati(String statName, long value) {
        ensureAnnounced(statName);
        channel.writeAndFlush(new StatIPrintMessage(ZonedDateTime.now(), statName, value));
    }

    public void candlef(String statName, ZonedDateTime openTs, ZonedDateTime closeTs, double open, double high, double low, double close) {
        ensureAnnounced(statName);
        channel.writeAndFlush(new CandlestickFPrintMessage(ZonedDateTime.now(), openTs, closeTs, statName, open, high, low, close));
    }

    public void candlei(String statName, ZonedDateTime openTs, ZonedDateTime closeTs, long open, long high, long low, long close) {
        ensureAnnounced(statName);
        channel.writeAndFlush(new CandlestickIPrintMessage(ZonedDateTime.now(), openTs, closeTs, statName, open, high, low, close));
    }

    @Override
    public void close() throws IOException {
        this.channel.close();
        this.workerGroup.shutdownGracefully();
    }

    private Bootstrap bootstrapClient(EventLoopGroup workerGroup) {
        Bootstrap b = new Bootstrap();
        b.group(workerGroup);
        b.option(ChannelOption.CONNECT_TIMEOUT_MILLIS, 1000);
        b.channel(NioSocketChannel.class);
        b.option(ChannelOption.SO_KEEPALIVE, true);
        b.handler(new EmitterClientChannelInitializer());
        return b;
    }

    public void flush() {
        channel.flush();
    }
}
