package cafe.josh.constellar.collectorui;

import cafe.josh.constellar.Log;
import org.eclipse.jetty.http.HttpMethod;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

public class RailsStyleRouter {
    private final Map<RailsStyleRoute, RouteHandler> routes;
    private final TemplateEngine templateEngine;

    public RailsStyleRouter(TemplateEngine templateEngine) {
        this.templateEngine = templateEngine;
        this.routes = new HashMap<>();
    }

    public void register(RailsStyleRoute route, RouteHandler listener) {
        routes.put(route, listener);
    }

    public void register(String railsStyleRoute, RouteHandler listener) {
        this.register(new RailsStyleRoute(railsStyleRoute), listener);
    }

    public void register(RouteHandler handler) {
        this.register(handler.getRoute(), handler);
    }

    public void route(HttpServletRequest request, HttpServletResponse response, ServletContext servletContext) throws Exception {
        String pathInfo = request.getPathInfo();
        if(pathInfo == null) {
            pathInfo = "/";
        }

        boolean routed = false;
        for (Map.Entry<RailsStyleRoute, RouteHandler> entry : routes.entrySet()) {
            Optional<Map<String, String>> match = entry.getKey().match(pathInfo);
            if(match.isPresent() && entry.getKey().getMethod() == HttpMethod.fromString(request.getMethod())) {
                if(routed) {
                    Log.warning("PathInfo " + pathInfo + " resolved to more than one router!");
                }
                routed = true;
                entry.getValue().handle(match.get(), request, response, templateEngine, new WebContext(request, response, servletContext, request.getLocale()), servletContext);
            }
        }
        if(!routed) {
            Log.warning("PathInfo didn't match any registered routes: " + pathInfo + " +" + HttpMethod.fromString(request.getMethod()));
        }
    }

}
