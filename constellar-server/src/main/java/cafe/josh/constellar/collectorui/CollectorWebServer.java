package cafe.josh.constellar.collectorui;

import cafe.josh.constellar.Log;
import cafe.josh.constellar.collector.Collector;
import cafe.josh.constellar.collectorui.servlet.CollectorHealthServlet;
import cafe.josh.constellar.collectorui.servlet.StatServlet;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.DefaultServlet;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.util.resource.Resource;
import org.postgresql.ds.PGConnectionPoolDataSource;

import java.sql.SQLException;

public class CollectorWebServer implements Runnable {
    public CollectorConnector collector;
    private final Thread thread;
    private final Server server;

    public CollectorWebServer(Collector collector, String dbHost, String dbName, String dbUser) {
        this.collector = collector;
        this.thread = new Thread(this);
        this.server = new Server(8080);

        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.setContextPath("/");
        context.setBaseResource(Resource.newClassPathResource("/web"));
        context.setAttribute("collector", collector);
        PGConnectionPoolDataSource dataSource = new PGConnectionPoolDataSource();
        dataSource.setDatabaseName(dbName);
        dataSource.setServerName(dbHost);
        dataSource.setUser(dbUser);
        try {
            dataSource.getPooledConnection();
        } catch (SQLException e) {
            Log.exception("Error testing getting connection to db.", e);
        }
        context.setAttribute("dbpool", dataSource);

        server.setHandler(context);

        context.addServlet(CollectorHealthServlet.class, "/health/*");
        context.addServlet(StatServlet.class, "/statistic/*");
        context.addServlet(DefaultServlet.class, "/css/*");
        context.addServlet(DefaultServlet.class, "/js/*");
    }

    @Override
    public void run() {
        try {
            server.start();
            server.join();
        } catch (Exception e) {
            Log.exception("Web server exited unexpectedly.", e);
        }
    }

    public void start() {
        thread.start();
    }

    public void stop() throws Exception {
        server.stop();
    }
}
