package cafe.josh.constellar.collectorui;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class HardcodedCollectorConnector implements CollectorConnector {
    @Override
    public List<LiveEmitter> getLiveEmitters() {
        List<LiveEmitter> ret = new ArrayList<>();
        ret.add(new LiveEmitter(UUID.randomUUID(), "testnode01.josh.cafe", null));
        ret.add(new LiveEmitter(UUID.randomUUID(), "testnode02.josh.cafe", null));
        ret.add(new LiveEmitter(UUID.randomUUID(), "testnode03.josh.cafe", null));
        return ret;
    }

    @Override
    public int getPendingWrites() {
        return 4;
    }

    @Override
    public boolean isConnectedToDatabase() {
        return true;
    }

    @Override
    public long getTotalWrites() {
        return 65743;
    }
}
