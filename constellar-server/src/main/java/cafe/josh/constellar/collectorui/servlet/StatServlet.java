package cafe.josh.constellar.collectorui.servlet;

import cafe.josh.constellar.collectorui.RailsStyleRouter;
import cafe.josh.constellar.collectorui.controller.statistic.StatsIndexHandler;
import cafe.josh.constellar.collectorui.controller.statistic.StatGraphHandler;

public class StatServlet extends BaseServlet {
    @Override
    protected void setupRoutes(RailsStyleRouter router) {
        router.register(new StatsIndexHandler());
        router.register(new StatGraphHandler());
    }
}
