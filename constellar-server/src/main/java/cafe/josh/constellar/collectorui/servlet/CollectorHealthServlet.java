package cafe.josh.constellar.collectorui.servlet;

import cafe.josh.constellar.collectorui.*;
import cafe.josh.constellar.collectorui.controller.health.HealthHandler;

public class CollectorHealthServlet extends BaseServlet {
    @Override
    protected void setupRoutes(RailsStyleRouter router) {
        router.register(new HealthHandler());
    }
}