package cafe.josh.constellar.collectorui;

import io.netty.channel.Channel;

import java.time.ZonedDateTime;
import java.util.Objects;
import java.util.UUID;

public class LiveEmitter implements Comparable<LiveEmitter> {
    private final UUID connectionId;
    private final String hostname;
    private final Channel channel;

    private ZonedDateTime lastHeartbeat;

    public LiveEmitter(UUID connectionId, String hostname, Channel channel) {
        this.connectionId = connectionId;
        this.hostname = hostname;
        this.channel = channel;
        this.lastHeartbeat = ZonedDateTime.now();
    }

    public String getHostname() {
        return hostname;
    }

    public UUID getConnectionId() {
        return connectionId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LiveEmitter that = (LiveEmitter) o;
        return Objects.equals(connectionId, that.connectionId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(connectionId);
    }

    @Override
    public int compareTo(LiveEmitter o) {
        if(this.hostname == null && o.hostname == null) {
            return 0;
        } else if(this.hostname == null) {
            return -1;
        } else if(o.hostname == null) {
            return 1;
        } else {
            return this.hostname.compareTo(o.hostname);
        }
    }

    public ZonedDateTime getLastHeartbeat() {
        return lastHeartbeat;
    }

    public void setLastHeartbeat(ZonedDateTime lastHeartbeat) {
        this.lastHeartbeat = lastHeartbeat;
    }

    public Channel getChannel() {
        return channel;
    }
}
