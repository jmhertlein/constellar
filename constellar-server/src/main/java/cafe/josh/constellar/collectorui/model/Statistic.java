package cafe.josh.constellar.collectorui.model;

import cafe.josh.constellar.util.StatType;
import cafe.josh.constellar.util.VisualizationType;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Statistic {
    private final String statName, prettyName;
    private final StatType statType;
    private final VisualizationType vizType;
    private final boolean hostSpecific;

    public Statistic(ResultSet rs) throws SQLException {
        this.statName = rs.getString("stat_name");
        this.prettyName = rs.getString("pretty_name");
        this.statType = StatType.match(rs.getString("stat_type"));
        this.vizType = VisualizationType.match(rs.getString("viz"));
        this.hostSpecific = rs.getBoolean("host_specific");
    }

    public String getStatName() {
        return statName;
    }

    public String getPrettyName() {
        return prettyName;
    }

    public StatType getStatType() {
        return statType;
    }

    public VisualizationType getVizType() {
        return vizType;
    }

    public boolean isHostSpecific() {
        return hostSpecific;
    }

    public String getTableName() {
        if(vizType == VisualizationType.LINE_CHART) {
            if(statType == StatType.FLOAT) {
                return "statf_print";
            } else if(statType == StatType.INT) {
                return "stati_print";
            }
        } else if (vizType == VisualizationType.CANDLESTICK_CHART) {
            if(statType == StatType.FLOAT) {
                return "candlestick_f_print";
            } else if(statType == StatType.INT) {
                return "candlestick_i_print";
            }
        }

        throw new RuntimeException("Unhandled combination: " + vizType.name() + " + " + statType.name());
    }
}
