package cafe.josh.constellar.collectorui.controller.statistic;

import cafe.josh.constellar.Log;
import cafe.josh.constellar.collector.model.StatFPrint;
import cafe.josh.constellar.collectorui.RailsStyleRoute;
import cafe.josh.constellar.collectorui.RouteHandler;
import cafe.josh.constellar.collectorui.model.Statistic;
import org.postgresql.ds.PGConnectionPoolDataSource;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.PooledConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class StatsIndexHandler implements RouteHandler {
    @Override
    public void handle(Map<String, String> urlPartParams,
                       HttpServletRequest request,
                       HttpServletResponse response,
                       TemplateEngine templates,
                       WebContext variables,
                       ServletContext servletContext) throws Exception {
        PGConnectionPoolDataSource dataSource = (PGConnectionPoolDataSource) servletContext.getAttribute("dbpool");
        List<Statistic> stats = new ArrayList<>();
        try {
            PooledConnection pooledConnection = dataSource.getPooledConnection();
            Connection conn = pooledConnection.getConnection();
            try (PreparedStatement stmt = conn.prepareStatement("select stat_name, pretty_name, stat_type, viz, host_specific from statistic")) {
                try (ResultSet rs = stmt.executeQuery()) {
                    while (rs.next()) {
                        stats.add(new Statistic(rs));
                    }
                }
            }
        } catch (SQLException e) {
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            Log.exception("SQLException", e);
            return;
        }

        variables.setVariable("stats", stats);
        templates.process("statistic/index", variables, response.getWriter());
    }

    @Override
    public String getRoute() {
        return "get /";
    }
}