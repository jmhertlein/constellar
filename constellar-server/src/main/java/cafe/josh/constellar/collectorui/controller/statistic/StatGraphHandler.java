package cafe.josh.constellar.collectorui.controller.statistic;

import cafe.josh.constellar.Log;
import cafe.josh.constellar.collector.model.*;
import cafe.josh.constellar.collectorui.RouteHandler;
import cafe.josh.constellar.collectorui.model.Queries;
import cafe.josh.constellar.collectorui.model.Statistic;
import cafe.josh.constellar.util.StatType;
import cafe.josh.constellar.util.VisualizationType;
import org.postgresql.ds.PGConnectionPoolDataSource;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.PooledConnection;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class StatGraphHandler implements RouteHandler {
    @Override
    public void handle(Map<String, String> urlPartParams, HttpServletRequest request, HttpServletResponse response, TemplateEngine templates, WebContext variables, ServletContext servletContext) throws Exception {
        String statName = urlPartParams.get("statName");
        String hostName = urlPartParams.get("hostName");
        
        PGConnectionPoolDataSource dataSource = (PGConnectionPoolDataSource) servletContext.getAttribute("dbpool");

        if(statName == null) {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            return;
        }

        if(hostName == null) {
            renderHostList(statName, dataSource, response, templates, variables);
            return;
        }

        renderChart(statName, hostName, dataSource, response, templates, variables);
    }

    private void renderHostList(String statName, PGConnectionPoolDataSource dataSource, HttpServletResponse response, TemplateEngine templates, WebContext variables) throws IOException {
        Optional<Statistic> statistic;
        Optional<List<String>> relevantHosts;
        try {
            PooledConnection pooledConnection = dataSource.getPooledConnection();
            Connection conn = pooledConnection.getConnection();
            statistic = Queries.getStatistic(conn, statName);
            if(statistic.isPresent()) {
                relevantHosts = Optional.of(Queries.getRelevantHosts(conn, statistic.get()));
            } else {
                relevantHosts = Optional.empty();
            }
        } catch (SQLException e) {
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            Log.exception("SQLException", e);
            return;
        }

        if(statistic.isPresent() && relevantHosts.isPresent()) {
            variables.setVariable("hosts", relevantHosts.get());
            variables.setVariable("stat", statistic.get());
            templates.process("statistic/chart_hosts", variables, response.getWriter());
        } else {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        }
    }

    @Override
    public String getRoute() {
        return "get /:statName/:hostName";
    }

    private static String getTableName(VisualizationType viz, StatType type) {
        StringBuilder ret = new StringBuilder();
        if(viz == VisualizationType.CANDLESTICK_CHART) {
            ret.append("candlestick_");
        } else if(viz == VisualizationType.LINE_CHART) {
            ret.append("stat");
        } else {
            throw new RuntimeException("Unhandled VisualizationType " + viz.name());
        }

        if(type == StatType.INT) {
            ret.append("i");
        } else if(type == StatType.FLOAT) {
            ret.append("f");
        } else {
            throw new RuntimeException("Unhandled StatType " + type.name());
        }

        ret.append("_print");
        return ret.toString();
    }

    private void renderChart(String statName, String hostName, PGConnectionPoolDataSource dataSource, HttpServletResponse response, TemplateEngine templates, WebContext variables) throws IOException {
        List<StatPrint> prints = new ArrayList<>();
        Statistic stat;
        try {
            PooledConnection pooledConnection = dataSource.getPooledConnection();
            Connection conn = pooledConnection.getConnection();

            try (PreparedStatement stmt = conn.prepareStatement("select * from statistic where stat_name=?")) {
                stmt.setString(1, statName);
                try(ResultSet rs = stmt.executeQuery()) {
                    if(rs.next()) {
                        stat = new Statistic(rs);

                        if(rs.next()) {
                            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                            Log.severe("More than one statistic found with stat_name=" + statName);
                            return;
                        }
                    } else {
                        response.setStatus(HttpServletResponse.SC_NOT_FOUND);
                        Log.warning("No statistic row found for stat_name=" + statName);
                        return;
                    }
                }
            } catch(SQLException e) {
                response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                Log.exception("SQLException", e);
                return;
            }

            try (PreparedStatement stmt = conn.prepareStatement("select * from " + stat.getTableName() + " where stat_name=? and host=? and mint_ts between now()-interval '1 hour' and now()")) {
                stmt.setString(1, statName);
                stmt.setString(2, hostName);
                try (ResultSet rs = stmt.executeQuery()) {
                    while (rs.next()) {
                        if(stat.getVizType() == VisualizationType.LINE_CHART) {
                            if(stat.getStatType() == StatType.INT) {
                                prints.add(new StatIPrint(rs));
                            } else if(stat.getStatType() == StatType.FLOAT) {
                                prints.add(new StatFPrint(rs));
                            } else {
                                throw new RuntimeException("Unhandled stat type: " + stat.getStatType());
                            }
                        } else if(stat.getVizType() == VisualizationType.CANDLESTICK_CHART) {
                            if(stat.getStatType() == StatType.INT) {
                                prints.add(new CandlestickStatIPrint(rs));
                            } else if(stat.getStatType() == StatType.FLOAT) {
                                prints.add(new CandlestickStatFPrint(rs));
                            } else {
                                throw new RuntimeException("Unhandled stat type: " + stat.getStatType());
                            }
                        } else {
                            throw new RuntimeException("Unhandled viz type: " + stat.getVizType());
                        }

                    }
                }
            }
        } catch (SQLException e) {
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            Log.exception("SQLException", e);
            return;
        }

        variables.setVariable("prints", prints);
        variables.setVariable("stat", stat);
        templates.process("statistic/chart", variables, response.getWriter());
    }
}