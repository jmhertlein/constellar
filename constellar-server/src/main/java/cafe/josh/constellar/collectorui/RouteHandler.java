package cafe.josh.constellar.collectorui;

import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

public interface RouteHandler {
    public void handle(Map<String, String> urlPartParams, HttpServletRequest request, HttpServletResponse response, TemplateEngine templates, WebContext variables, ServletContext servletContext) throws Exception;
    public String getRoute();
}
