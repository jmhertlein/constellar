package cafe.josh.constellar.collectorui.model;

import cafe.josh.constellar.Log;

import javax.servlet.http.HttpServletResponse;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Queries {
    private Queries() {}

    public static Optional<Statistic> getStatistic(Connection conn, String statName) throws SQLException {
        try (PreparedStatement stmt = conn.prepareStatement("select * from statistic where stat_name=?")) {
            stmt.setString(1, statName);
            try (ResultSet rs = stmt.executeQuery()) {
                if (rs.next()) {
                    Statistic stat = new Statistic(rs);

                    if (rs.next()) {
                        Log.severe("More than one statistic found with stat_name=" + statName);
                    }

                    return Optional.of(stat);
                } else {
                    Log.warning("No statistic row found for stat_name=" + statName);
                    return Optional.empty();
                }
            }
        }
    }

    public static List<String> getRelevantHosts(Connection conn, Statistic stat) throws SQLException {
        List<String> ret = new ArrayList<>();
        try (PreparedStatement stmt = conn.prepareStatement("select distinct host from " + stat.getTableName() + " where stat_name=? order by host asc")) {
            stmt.setString(1, stat.getStatName());
            try (ResultSet rs = stmt.executeQuery()) {
                while(rs.next()) {
                    ret.add(rs.getString(1));
                }
            }
        }

        return ret;
    }
}
