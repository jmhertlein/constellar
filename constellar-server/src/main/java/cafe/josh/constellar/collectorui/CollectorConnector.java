package cafe.josh.constellar.collectorui;

import java.util.List;

public interface CollectorConnector {
    public List<LiveEmitter> getLiveEmitters();
    public int getPendingWrites();
    public boolean isConnectedToDatabase();
    public long getTotalWrites();
}
