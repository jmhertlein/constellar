package cafe.josh.constellar.collectorui.controller.health;

import cafe.josh.constellar.collectorui.CollectorConnector;
import cafe.josh.constellar.collectorui.RouteHandler;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

public class HealthHandler implements RouteHandler {
    @Override
    public void handle(Map<String, String> urlPartParams, HttpServletRequest request, HttpServletResponse response, TemplateEngine templates, WebContext variables, ServletContext servletContext) throws Exception {
        CollectorConnector collector = (CollectorConnector) servletContext.getAttribute("collector");
        variables.setVariable("liveEmitters", collector.getLiveEmitters());
        variables.setVariable("pendingWrites", collector.getPendingWrites());
        variables.setVariable("totalDbWrites", collector.getTotalWrites());
        variables.setVariable("isConnectedToDatabase", collector.isConnectedToDatabase());
        templates.process("health/index", variables, response.getWriter());
    }

    @Override
    public String getRoute() {
        return "get /";
    }
}