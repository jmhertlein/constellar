package cafe.josh.constellar.collectorui.servlet;

import cafe.josh.constellar.Log;
import cafe.josh.constellar.collectorui.RailsStyleRouter;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ServletContextTemplateResolver;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public abstract class BaseServlet extends HttpServlet {
    private RailsStyleRouter router;

    public BaseServlet() {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doAllMethods(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doAllMethods(request, response);
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doAllMethods(req, resp);
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doAllMethods(req, resp);
    }

    private void doAllMethods(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");

        try {
            getRouter().route(request, response, getServletContext());
        } catch (Exception e) {
            Log.exception("Error while routing request: " + request.getPathInfo(), e);
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }

    protected abstract void setupRoutes(RailsStyleRouter router);

    public RailsStyleRouter getRouter() {
        if(router == null) {
            ServletContextTemplateResolver templateResolver = new ServletContextTemplateResolver(this.getServletContext());
            templateResolver.setTemplateMode(TemplateMode.HTML);
            templateResolver.setPrefix("/html/");
            templateResolver.setSuffix(".html");
            templateResolver.setCacheTTLMs(3600000L);
            templateResolver.setCacheable(true);
            TemplateEngine templateEngine = new TemplateEngine();
            templateEngine.setTemplateResolver(templateResolver);
            router = new RailsStyleRouter(templateEngine);
            setupRoutes(router);
        }

        return router;
    }
}
