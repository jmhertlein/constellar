package cafe.josh.constellar.collectorui;

import org.eclipse.jetty.http.HttpMethod;

import java.util.*;

public class RailsStyleRoute {
    private final HttpMethod method;
    private final List<RouteNode> route;

    public RailsStyleRoute(String routeSpec) {
        route = new ArrayList<>();

        String[] splitOnSpace = routeSpec.split(" ");
        if(splitOnSpace.length != 2) {
            throw new RuntimeException("RailsStyleRoute is malformed, must be of format: [http verb] [route spec]");
        }
        method = HttpMethod.fromString(splitOnSpace[0].toUpperCase());
        if(method == null) {
            throw new RuntimeException("RailsStyleRoute HttpMethod is not valid: " + splitOnSpace[0]);
        }

        for(String part : splitOnSpace[1].split("/")) {
            if(part.isEmpty()) {
                continue;
            }

            if(part.startsWith(":")) {
                route.add(new SimpleParameterRouteNode(part.substring(1)));
            } else {
                route.add(new StringRouteNode(part));
            }
        }
    }

    public Optional<Map<String, String>> match(String path) {
        Map<String, String> params = new HashMap<>();
        Iterator<RouteNode> routeIterator = route.iterator();
        for(String part : path.split("/")) {
            if(part.isEmpty()) {
                continue;
            }

            if(!routeIterator.hasNext()) {
                return Optional.empty();
            }

            RouteNode next = routeIterator.next();

            if(next.matches(part)) {
                if(next.isParameter()) {
                    params.put(next.getParameterKey(), part);
                }
            } else {
                return Optional.empty();
            }
        }
        if(routeIterator.hasNext() && !routeIterator.next().isParameter()) {
            return Optional.empty();
        } else {
            return Optional.of(params);
        }
    }

    public HttpMethod getMethod() {
        return method;
    }

    private interface RouteNode {
        public boolean matches(String part);
        public default boolean isParameter() {
            return false;
        }
        public default String getParameterKey() {
            throw new UnsupportedOperationException("Unsupported.");
        }
    }

    private static class StringRouteNode implements RouteNode {
        private final String lexeme;

        public StringRouteNode(String lexeme) {
            this.lexeme = lexeme;
        }

        @Override
        public boolean matches(String part) {
            return lexeme.equals(part);
        }
    }

    private static class SimpleParameterRouteNode implements RouteNode {
        private final String key;

        public SimpleParameterRouteNode(String key) {
            this.key = key;
        }

        @Override
        public boolean matches(String part) {
            return !part.isEmpty();
        }

        @Override
        public boolean isParameter() {
            return true;
        }

        @Override
        public String getParameterKey() {
            return key;
        }
    }
}
