package cafe.josh.constellar.collector.model;

import cafe.josh.constellar.Received;
import cafe.josh.constellar.message.clientemitter.LogLevel;
import cafe.josh.constellar.message.emittercollector.RelayedLogPrintMessage;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Objects;

public class LogPrint implements ModelRow {
    private final LogLevel level;
    private final String message;
    private final String serviceName;
    private final String host;
    private final ZonedDateTime rcvTs, mintTs;

    public LogPrint(ResultSet rs) throws SQLException {
        this.level = LogLevel.valueOf(rs.getString("level"));
        this.message = rs.getString("message");
        this.serviceName = rs.getString("service_name");
        this.host = rs.getString("host");
        this.rcvTs = rs.getObject("rcv_ts", OffsetDateTime.class).atZoneSameInstant(ZoneId.systemDefault());
        this.mintTs = rs.getObject("mint_ts", OffsetDateTime.class).atZoneSameInstant(ZoneId.systemDefault());
    }

    public LogPrint(Received<RelayedLogPrintMessage> m) {
        RelayedLogPrintMessage o = m.getReceived();

        this.level = o.getLevel();
        this.message = o.getMessage();
        this.serviceName = o.getServiceName();
        this.host = m.getHost();
        this.rcvTs = m.getRcvTs();
        this.mintTs = o.getMintTs();
    }

    public LogLevel getLevel() {
        return level;
    }

    public String getMessage() {
        return message;
    }

    public String getServiceName() {
        return serviceName;
    }

    public String getHost() {
        return host;
    }

    public ZonedDateTime getRcvTs() {
        return rcvTs;
    }

    public ZonedDateTime getMintTs() {
        return mintTs;
    }

    @Override
    public void toPreparedStatement(PreparedStatement ps) throws SQLException {
        ps.setObject(1, mintTs.toOffsetDateTime().withOffsetSameInstant(ZoneOffset.UTC));
        ps.setObject(2, rcvTs.toOffsetDateTime().withOffsetSameInstant(ZoneOffset.UTC));
        ps.setString(3, serviceName);
        ps.setString(4, message);
        ps.setString(5, level.name().toLowerCase());
        ps.setString(6, host);
    }

    @Override
    public String getInsertStatement() {
        return "insert into log_print(mint_ts, rcv_ts, service_name, message, level, host) values (?, ?, ?, ?, ?::log_level, ?)";
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.level);
        hash = 97 * hash + Objects.hashCode(this.message);
        hash = 97 * hash + Objects.hashCode(this.serviceName);
        hash = 97 * hash + Objects.hashCode(this.host);
        hash = 97 * hash + Objects.hashCode(this.rcvTs);
        hash = 97 * hash + Objects.hashCode(this.mintTs);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) {
            return true;
        }
        if(obj == null) {
            return false;
        }
        if(getClass() != obj.getClass()) {
            return false;
        }
        final LogPrint other = (LogPrint) obj;
        if(!Objects.equals(this.message, other.message)) {
            return false;
        }
        if(!Objects.equals(this.serviceName, other.serviceName)) {
            return false;
        }
        if(!Objects.equals(this.host, other.host)) {
            return false;
        }
        if(this.level != other.level) {
            return false;
        }
        if(!Objects.equals(this.rcvTs, other.rcvTs)) {
            return false;
        }
        if(!Objects.equals(this.mintTs, other.mintTs)) {
            return false;
        }
        return true;
    }
}
