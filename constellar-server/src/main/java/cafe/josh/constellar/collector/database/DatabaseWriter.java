package cafe.josh.constellar.collector.database;

import cafe.josh.constellar.Log;
import cafe.josh.constellar.collector.PreparedStatementManager;
import cafe.josh.constellar.collector.model.ModelRow;

import java.sql.*;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;

public class DatabaseWriter implements Runnable {
    private static final int MAX_TASK_RETRIES = 3;
    private final Thread thread;
    private final String url, username;
    private final LinkedBlockingQueue<DatabaseTask> queue;
    private final AtomicBoolean stop;

    private DatabaseTask task;
    private int currentTaskTries;
    private int triesSinceLastConnect;
    private boolean connectedToDatabase;
    private long totalDbWrites;

    public DatabaseWriter(String url, String username) {
        this.thread = new Thread(this);
        this.url = url;
        this.username = username;
        this.queue = new LinkedBlockingQueue<>();
        this.stop = new AtomicBoolean(false);
        this.triesSinceLastConnect = 0;
        this.connectedToDatabase = false;
        this.totalDbWrites = 0;
    }

    @Override
    public void run() {
        reconnect:
        while(!stop.get()) {
            Log.info("Connecting to db at " + url + " as " + username);
            triesSinceLastConnect++;
            if(triesSinceLastConnect > 1) {
                Log.warning("Try #" + triesSinceLastConnect + ". Sleeping first.");
                try {
                    Thread.sleep(Math.min(10, triesSinceLastConnect) * 1000);
                } catch (InterruptedException e) {
                    Log.warning("Interrupted while sleeping before reconnect.");
                }
            }
            try (Connection conn = DriverManager.getConnection(url, username, null);
                 PreparedStatementManager psManager = new PreparedStatementManager(conn)) {
                connectedToDatabase = true;
                triesSinceLastConnect = 0;
                Log.info("DBWriter connected to " + url + " as " + username);
                while (!this.stop.get()) {
                    if(task == null || currentTaskTries > MAX_TASK_RETRIES) {
                        try {
                            if(task != null) {
                                Log.warning("Tried " + currentTaskTries + " times to handle " + task + ", giving up and moving on.");
                            }
                            task = queue.take();
                            currentTaskTries = 0;
                        } catch(InterruptedException e) {
                            Log.warning("Interrupted while take()ing from queue.");
                        }
                    } else {
                        Log.warning("Inserting task leftover from previous exception.");
                    }
                    Log.debug("DBwriter unblocked.");
                    currentTaskTries++;
                    task.execute(conn, psManager);
                    totalDbWrites++;
                    Log.debug("DB Write: " + task);
                    task = null;
                }
            } catch (SQLException e) {
                Log.exception("Exception interrupted db connection. ", e);
                if(task != null) {
                    Log.severe("Row being handled during exception: " + task);
                }
                continue reconnect;
            } catch (Exception e) {
                Log.exception("Failed to connect to database.", e);
                continue reconnect;
            } finally {
                connectedToDatabase = false;
            }
        }
    }

    public void persist(ModelRow m) {
        this.queue.add(new PersistModelObjectTask(m));
    }

    public void submit(DatabaseTask t) {
        this.queue.add(t);
    }

    public AtomicBoolean getStop() {
        return stop;
    }

    public void start() {
        thread.start();
    }

    public void interrupt() {
        thread.interrupt();
    }

    public void join() throws InterruptedException {
        thread.join();
    }

    public int getQueueDepth() {
        return queue.size();
    }

    public boolean isConnectedToDatabase() {
        return connectedToDatabase;
    }

    public long getTotalDbWrites() {
        return totalDbWrites;
    }
}
