package cafe.josh.constellar.collector.database;

import cafe.josh.constellar.collector.PreparedStatementManager;

import java.sql.Connection;
import java.sql.SQLException;

public interface DatabaseTask {
    public void execute(Connection conn, PreparedStatementManager psMan) throws SQLException;
}