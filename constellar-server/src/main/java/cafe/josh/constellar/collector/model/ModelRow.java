package cafe.josh.constellar.collector.model;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public interface ModelRow {
    public void toPreparedStatement(PreparedStatement ps) throws SQLException;

    public String getInsertStatement();
}
