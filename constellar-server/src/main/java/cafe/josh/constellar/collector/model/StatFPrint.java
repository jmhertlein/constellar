package cafe.josh.constellar.collector.model;

import cafe.josh.constellar.Received;
import cafe.josh.constellar.message.emittercollector.RelayedStatFPrintMessage;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.ZonedDateTime;

public class StatFPrint extends StatPrint {
    private final double value;

    public StatFPrint(ResultSet rs) throws SQLException {
        super(rs);
        this.value = rs.getDouble("value");
    }

    public StatFPrint(ZonedDateTime mintTs, ZonedDateTime rcvTs, String statName, double value, String host) {
        super(mintTs, rcvTs, statName, host);
        this.value = value;
    }

    public StatFPrint(Received<RelayedStatFPrintMessage> m) {
        this(m.getReceived().getMintTs(),
                m.getRcvTs(),
                m.getReceived().getStatName(),
                m.getReceived().getValue(),
                m.getHost());
    }

    @Override
    public String getInsertStatement() {
        return "insert into statf_print (mint_ts, rcv_ts, stat_name, host, value) values (?, ?, ?, ?, ?)";
    }

    @Override
    protected void valueToPreparedStatement(PreparedStatement ps) throws SQLException {
        ps.setDouble(5, value);
    }

    public double getValue() {
        return value;
    }
}
