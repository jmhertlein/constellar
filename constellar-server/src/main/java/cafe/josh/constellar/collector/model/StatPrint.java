package cafe.josh.constellar.collector.model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public abstract class StatPrint implements ModelRow {
    private final ZonedDateTime mintTs, rcvTs;
    private final String statName;
    private final String host;

    public StatPrint(ZonedDateTime mintTs, ZonedDateTime rcvTs, String statName, String host) {
        this.mintTs = mintTs;
        this.rcvTs = rcvTs;
        this.statName = statName;
        this.host = host;
    }
    
    public StatPrint(ResultSet rs) throws SQLException {
        this(rs.getObject("mint_ts", OffsetDateTime.class).atZoneSameInstant(ZoneId.systemDefault()),
             rs.getObject("rcv_ts", OffsetDateTime.class).atZoneSameInstant(ZoneId.systemDefault()),
             rs.getString("stat_name"),
             rs.getString("host")
            );
    }

    @Override
    public final void toPreparedStatement(PreparedStatement ps) throws SQLException {
        ps.setObject(1, mintTs.toOffsetDateTime().withOffsetSameInstant(ZoneOffset.UTC));
        ps.setObject(2, rcvTs.toOffsetDateTime().withOffsetSameInstant(ZoneOffset.UTC));
        ps.setString(3, statName);
        ps.setString(4, host);
        this.valueToPreparedStatement(ps);
    }
    
    protected abstract void valueToPreparedStatement(PreparedStatement ps) throws SQLException;

    public ZonedDateTime getMintTs() {
        return mintTs;
    }

    public String getJSFormattedMintTs() {
        return mintTs.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME);
    }

    public ZonedDateTime getRcvTs() {
        return rcvTs;
    }

    public String getStatName() {
        return statName;
    }

    public String getHost() {
        return host;
    }
}
