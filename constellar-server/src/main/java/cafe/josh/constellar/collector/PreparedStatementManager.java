package cafe.josh.constellar.collector;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class PreparedStatementManager implements AutoCloseable {
    private final Connection conn;
    private final Map<String, PreparedStatement> statements;

    public PreparedStatementManager(Connection conn) {
        this.conn = conn;
        this.statements = new HashMap<>();
    }

    @Override
    public void close() throws Exception {
        for(PreparedStatement e : statements.values()) {
            e.close();
        }
    }
    
    public PreparedStatement getCachedStatementFor(String stmt) throws SQLException {
        PreparedStatement ps = statements.get(stmt);
        if(ps == null) {
            ps = conn.prepareStatement(stmt);
            statements.put(stmt, ps);
        }
        return ps;
    }
}
