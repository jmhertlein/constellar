package cafe.josh.constellar.collector.database;

import cafe.josh.constellar.collector.PreparedStatementManager;
import cafe.josh.constellar.collector.model.ModelRow;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class PersistModelObjectTask implements DatabaseTask {
    private final ModelRow row;

    public PersistModelObjectTask(ModelRow row) {
        this.row = row;
    }

    @Override
    public void execute(Connection conn, PreparedStatementManager psMan) throws SQLException {
        PreparedStatement ps = psMan.getCachedStatementFor(row.getInsertStatement());
        ps.clearParameters();
        row.toPreparedStatement(ps);
        ps.executeUpdate();
    }
}
