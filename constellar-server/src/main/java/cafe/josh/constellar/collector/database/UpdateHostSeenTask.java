package cafe.josh.constellar.collector.database;

import cafe.josh.constellar.Log;
import cafe.josh.constellar.collector.PreparedStatementManager;
import cafe.josh.constellar.message.util.MacAddress;

import java.net.InetAddress;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class UpdateHostSeenTask implements DatabaseTask {
    private final String hostname;
    private final MacAddress mac;
    private final InetAddress lastIp;
    private final ZonedDateTime lastSeenTs;
    private final int cpuCount;
    private final long memBytes;

    public UpdateHostSeenTask(String hostname, MacAddress mac, InetAddress lastIp, ZonedDateTime lastSeenTs, int cpuCount, long memBytes) {
        this.hostname = hostname;
        this.mac = mac;
        this.lastIp = lastIp;
        this.lastSeenTs = lastSeenTs;
        this.cpuCount = cpuCount;
        this.memBytes = memBytes;
    }

    @Override
    public void execute(Connection conn, PreparedStatementManager psMan) throws SQLException {
        PreparedStatement ps = psMan.getCachedStatementFor(
                "insert into host (hostname, mac, last_ip, last_seen, cpu_count, mem_bytes) " +
                        "values (?, ?::macaddr, ?::inet, ?, ?, ?) on conflict (hostname) do update " +
                        "set hostname=?, mac=?::macaddr, last_ip=?::inet, last_seen=?, cpu_count=?, mem_bytes=?");
        ps.clearParameters();
        ps.setString(1, hostname);
        ps.setString(2, mac.toString());
        ps.setString(3, lastIp.getHostAddress());
        ps.setObject(4, lastSeenTs.toOffsetDateTime().withOffsetSameInstant(ZoneOffset.UTC));
        ps.setInt(5, cpuCount);
        ps.setLong(6, memBytes);

        ps.setString(7, hostname);
        ps.setString(8, mac.toString());
        ps.setString(9, lastIp.getHostAddress());
        ps.setObject(10, lastSeenTs.toOffsetDateTime().withOffsetSameInstant(ZoneOffset.UTC));
        ps.setInt(11, cpuCount);
        ps.setLong(12, memBytes);
        int rows = ps.executeUpdate();
        Log.info("UPSERT " + this.toString() + " --- " + rows + " rows affected.");
    }

    @Override
    public String toString() {
        return "{" +
                "hostname='" + hostname + '\'' +
                ", mac=" + mac +
                ", lastIp=" + lastIp +
                ", lastSeenTs=" + lastSeenTs +
                ", cpuCount=" + cpuCount +
                ", memBytes=" + memBytes +
                '}';
    }
}
