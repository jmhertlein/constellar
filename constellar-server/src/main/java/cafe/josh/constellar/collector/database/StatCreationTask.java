package cafe.josh.constellar.collector.database;

import cafe.josh.constellar.Log;
import cafe.josh.constellar.collector.PreparedStatementManager;
import cafe.josh.constellar.message.emittercollector.RelayedStatAnnouncementMessage;
import cafe.josh.constellar.util.StatType;
import cafe.josh.constellar.util.VisualizationType;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class StatCreationTask implements DatabaseTask {
    private final String statName, prettyName;
    private final StatType type;
    private final VisualizationType vizType;
    private final boolean hostSpecific;

    public StatCreationTask(String statName, String prettyName, StatType type, VisualizationType vizType, boolean hostSpecific) {
        this.statName = statName;
        this.prettyName = prettyName;
        this.type = type;
        this.vizType = vizType;
        this.hostSpecific = hostSpecific;
    }

    public StatCreationTask(RelayedStatAnnouncementMessage m) {
        this(m.getStatName(), m.getPrettyName(), m.getStatType(), m.getVizType(), m.isHostSpecific());
    }

    @Override
    public void execute(Connection conn, PreparedStatementManager psMan) throws SQLException {
        PreparedStatement ps = psMan.getCachedStatementFor("insert into statistic (stat_name, pretty_name, stat_type, viz, host_specific) values (?, ?, ?::stat_type, ?::visualization_type, ?) on conflict do nothing");
        ps.clearParameters();
        ps.setString(1, statName);
        ps.setString(2, prettyName);
        ps.setString(3, type.name().toLowerCase());
        ps.setString(4, vizType.name().toLowerCase());
        ps.setBoolean(5, hostSpecific);
        int rows = ps.executeUpdate();
        Log.info("INSERT " + this.toString() + " --- " + rows + " rows affected.");
    }

    @Override
    public String toString() {
        return "{" +
                "statName='" + statName + '\'' +
                ", type=" + type +
                ", vizType=" + vizType +
                ", hostSpecific=" + hostSpecific +
                '}';
    }
}
