package cafe.josh.constellar.collector;

import cafe.josh.constellar.Log;
import cafe.josh.constellar.Received;
import cafe.josh.constellar.collector.database.DatabaseWriter;
import cafe.josh.constellar.collector.database.StatCreationTask;
import cafe.josh.constellar.collector.database.UpdateHostSeenTask;
import cafe.josh.constellar.collector.model.*;
import cafe.josh.constellar.collectorui.LiveEmitter;
import cafe.josh.constellar.message.emittercollector.*;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.ssl.SslHandler;

import java.net.InetSocketAddress;
import java.time.ZonedDateTime;
import java.util.Set;
import java.util.UUID;

public class CollectorHandler extends ChannelInboundHandlerAdapter {
    private final DatabaseWriter dbWriter;
    private final Set<LiveEmitter> emitters;
    private final UUID connectionId;
    private LiveEmitter emitter;

    CollectorHandler(DatabaseWriter dbWriter, Set<LiveEmitter> emitters) {
        this.dbWriter = dbWriter;
        this.emitters = emitters;
        this.connectionId = UUID.randomUUID();
        this.emitter = null;
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        EmitterCollectorMessage m = (EmitterCollectorMessage) msg;

        switch(m.getMessageType()) {
            case EMITTER_HELLO:
                EmitterHelloMessage eHM = ((EmitterHelloMessage) msg);
                Log.info("HELLO: " + eHM.getHostname() + "(" + ctx.pipeline().get(SslHandler.class).engine().getSession().getCipherSuite() + ")");
                this.emitter = new LiveEmitter(connectionId, eHM.getHostname(), ctx.channel());
                emitters.add(this.emitter);
                dbWriter.submit(new UpdateHostSeenTask(eHM.getHostname(), eHM.getMacAddress(), ((InetSocketAddress)ctx.channel().remoteAddress()).getAddress(), ZonedDateTime.now(), eHM.getCpuCount(), eHM.getMemBytes()));
                break;
            case RELAYED_STAT_ANNOUNCEMENT:
                dbWriter.submit(new StatCreationTask((RelayedStatAnnouncementMessage) msg));
                break;
            case RELAYED_LOG_PRINT:
                dbWriter.persist(new LogPrint(new Received<>((RelayedLogPrintMessage) msg, this.emitter.getHostname())));
                Log.debug("Log print persisted.");
                break;
            case RELAYED_STATF_PRINT:
                dbWriter.persist(new StatFPrint(new Received<>((RelayedStatFPrintMessage) msg, this.emitter.getHostname())));
                Log.debug("Statf persisted.");
                break;
            case RELAYED_STATI_PRINT:
                dbWriter.persist(new StatIPrint(new Received<>((RelayedStatIPrintMessage) msg, this.emitter.getHostname())));
                Log.debug("Stati persisted.");
                break;
            case RELAYED_CANDLESTICK_I_PRINT:
                dbWriter.persist(new CandlestickStatIPrint(new Received<>((RelayedCandlestickIPrintMessage) msg, this.emitter.getHostname())));
                break;
            case RELAYED_CANDLESTICK_F_PRINT:
                dbWriter.persist(new CandlestickStatFPrint(new Received<>((RelayedCandlestickFPrintMessage) msg, this.emitter.getHostname())));
                break;
            case EMITTER_HEARTBEAT:
                Log.debug("[Heartbeat]: " + this.emitter.getHostname());
                this.emitter.setLastHeartbeat(((EmitterHeartbeatMessage) msg).getMintTs());
                break;
            default:
                throw new UnsupportedOperationException("Unhandled enum value: " + m.getMessageType().name());
        }

        if(m.getMessageType().isAckable()) {
            ctx.channel().writeAndFlush(new PrintAckMessage(m.getId()));
            Log.debug("ACK: " + m.getId());
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        Log.exception("Exception caught.", cause);
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        emitters.remove(this.emitter);
    }

    @Override
    public boolean isSharable() {
        return false;
    }
}
