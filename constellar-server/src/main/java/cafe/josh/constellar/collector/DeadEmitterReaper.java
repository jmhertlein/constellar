package cafe.josh.constellar.collector;

import cafe.josh.constellar.Log;
import cafe.josh.constellar.collectorui.LiveEmitter;

import java.time.Duration;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Set;

public class DeadEmitterReaper implements Runnable {
    public static final Duration EMITTER_DEATH_AGE = Duration.of(1, ChronoUnit.MINUTES);

    private final Set<LiveEmitter> emitters;

    public DeadEmitterReaper(Set<LiveEmitter> emitters) {
        this.emitters = emitters;
    }

    @Override
    public void run() {
        Log.debug("Checking for dead emitters...");
        for (LiveEmitter emitter : this.emitters) {
            ZonedDateTime threshold = ZonedDateTime.now().minus(EMITTER_DEATH_AGE);
            if(emitter.getLastHeartbeat().isBefore(threshold)) {
                Log.warning("Emitter timed out: " + emitter + ". Reaping.");
                emitter.getChannel().close();
            }
        }
    }
}
