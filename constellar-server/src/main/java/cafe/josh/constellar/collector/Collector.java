package cafe.josh.constellar.collector;

import cafe.josh.constellar.Log;
import cafe.josh.constellar.collector.database.DatabaseWriter;
import cafe.josh.constellar.collectorui.CollectorConnector;
import cafe.josh.constellar.collectorui.LiveEmitter;
import cafe.josh.constellar.util.ConfigFinder;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.ssl.ClientAuth;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;
import io.netty.handler.ssl.util.SelfSignedCertificate;

import javax.net.ssl.SSLException;
import java.io.File;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Collector implements CollectorConnector {
    public static final int STANDARD_PORT = 42524;
    public static final int DEAD_EMITTER_CHECK_DELAY_SECONDS = 4;

    private final int port;
    private final DatabaseWriter dbWriter;
    private final Set<LiveEmitter> emitters;
    private final ScheduledExecutorService scheduledPool;
    private Channel channel;

    private final DeadEmitterReaper deadEmitterReaper;

    public Collector(int listenPort, String dbHost, int dbPort, String dbName, String dbUser) {
        this.port = listenPort;
        channel = null;
        dbWriter = new DatabaseWriter("jdbc:postgresql://" + dbHost + ":" + dbPort + "/" + dbName, dbUser);
        emitters = Collections.newSetFromMap(new ConcurrentHashMap<>());

        scheduledPool = Executors.newSingleThreadScheduledExecutor();

        deadEmitterReaper = new DeadEmitterReaper(emitters);
        scheduledPool.scheduleWithFixedDelay(deadEmitterReaper, DEAD_EMITTER_CHECK_DELAY_SECONDS, DEAD_EMITTER_CHECK_DELAY_SECONDS, TimeUnit.SECONDS);
    }

    public void run() throws Exception {
        Log.info("Running collector.");
        Class.forName("org.postgresql.Driver");

        dbWriter.start();
        EventLoopGroup bossGroup = new NioEventLoopGroup();
        EventLoopGroup workerGroup = new NioEventLoopGroup();
        try {
            ServerBootstrap b = bootstrap(bossGroup, workerGroup);
            Log.info("Binding to port " + port);
            channel = b.bind(port).channel();
            channel.closeFuture().sync();
            Log.info("Port unbound.");
        } finally {
            workerGroup.shutdownGracefully();
            bossGroup.shutdownGracefully();
        }
    }

    private ServerBootstrap bootstrap(EventLoopGroup bossGroup, EventLoopGroup workerGroup) throws CertificateException, SSLException {
        SslContext sslCtx = SslContextBuilder.forServer(ConfigFinder.SYS_COLLECTOR_CERT_PATH.toFile(),
                ConfigFinder.SYS_COLLECTOR_KEY_PATH.toFile())
                .clientAuth(ClientAuth.REQUIRE)
                .trustManager(ConfigFinder.SYS_CA_CERT_PATH.toFile()).build();

        ServerBootstrap b = new ServerBootstrap();
        b.group(bossGroup, workerGroup)
                .channel(NioServerSocketChannel.class)
                .childHandler(new CollectorChannelInitializer(sslCtx, dbWriter, emitters))
                .option(ChannelOption.SO_BACKLOG, 128)
                .childOption(ChannelOption.SO_KEEPALIVE, true);
        return b;
    }

    public void stop() throws InterruptedException {
        Log.raw("Stopping collector.");
        if(channel != null) {
            Log.info("Closing socket.");
            channel.close();
        }
        Log.raw("Stopping db writer.");
        dbWriter.getStop().set(true);
        dbWriter.interrupt();
        Log.raw("Waiting for dbWriter to flush.");
        dbWriter.join();
        Log.raw("dbWriter flushed.");
    }

    @Override
    public List<LiveEmitter> getLiveEmitters() {
        List<LiveEmitter> liveEmitters = new ArrayList<>(emitters);
        Collections.sort(liveEmitters);
        return liveEmitters;
    }

    @Override
    public int getPendingWrites() {
        return dbWriter.getQueueDepth();
    }

    @Override
    public boolean isConnectedToDatabase() {
        return dbWriter.isConnectedToDatabase();
    }

    @Override
    public long getTotalWrites() {
        return dbWriter.getTotalDbWrites();
    }
}
