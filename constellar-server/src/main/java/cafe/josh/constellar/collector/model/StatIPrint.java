package cafe.josh.constellar.collector.model;

import cafe.josh.constellar.Received;
import cafe.josh.constellar.message.emittercollector.RelayedStatIPrintMessage;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.ZonedDateTime;

public class StatIPrint extends StatPrint {
    private final long value;

    public StatIPrint(ResultSet rs) throws SQLException {
        super(rs);
        this.value = rs.getLong("value");
    }

    public StatIPrint(Received<RelayedStatIPrintMessage> m) {
        this(m.getReceived().getMintTs(),
                m.getRcvTs(),
                m.getReceived().getStatName(),
                m.getReceived().getValue(),
                m.getHost());
    }

    public StatIPrint(ZonedDateTime mintTs, ZonedDateTime rcvTs, String statName, long value, String host) {
        super(mintTs, rcvTs, statName, host);
        this.value = value;
    }

    public long getValue() {
        return value;
    }

    @Override
    public String getInsertStatement() {
        return "insert into stati_print (mint_ts, rcv_ts, stat_name, host, value) values (?, ?, ?, ?, ?)";
    }

    @Override
    protected void valueToPreparedStatement(PreparedStatement ps) throws SQLException {
        ps.setLong(5, value);
    }

}
