package cafe.josh.constellar.collector;

import cafe.josh.constellar.Log;
import cafe.josh.constellar.collector.database.DatabaseWriter;
import cafe.josh.constellar.collectorui.LiveEmitter;
import cafe.josh.constellar.util.EmitterCollectorJsonParserCodec;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.json.JsonObjectDecoder;
import io.netty.handler.ssl.SslContext;

import java.util.Set;

public class CollectorChannelInitializer extends ChannelInitializer<SocketChannel> {
    private final SslContext sslCtx;
    private final DatabaseWriter dbWriter;
    private final Set<LiveEmitter> emitters;

    CollectorChannelInitializer(SslContext sslCtx, DatabaseWriter dbWriter, Set<LiveEmitter> emitters) {
        this.sslCtx = sslCtx;
        this.dbWriter = dbWriter;
        this.emitters = emitters;
    }

    @Override
    protected void initChannel(SocketChannel socketChannel) throws Exception {
        Log.debug("Initializing channel: " + socketChannel.remoteAddress());
        ChannelPipeline p = socketChannel.pipeline();
        p.addLast(sslCtx.newHandler(socketChannel.alloc()));
        p.addLast(new JsonObjectDecoder());
        p.addLast(new EmitterCollectorJsonParserCodec());
        p.addLast(new JsonObjectToCollectorEmitterDecoder());
        p.addLast(new CollectorHandler(dbWriter, emitters));
    }


}
