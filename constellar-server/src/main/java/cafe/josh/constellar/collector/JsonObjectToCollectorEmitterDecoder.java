package cafe.josh.constellar.collector;

import cafe.josh.constellar.Log;
import cafe.josh.constellar.message.clientemitter.StatAnnouncementMessage;
import cafe.josh.constellar.message.emittercollector.*;
import com.google.gson.JsonObject;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageDecoder;

import java.util.List;

public class JsonObjectToCollectorEmitterDecoder extends MessageToMessageDecoder<JsonObject> {
    @Override
    protected void decode(ChannelHandlerContext channelHandlerContext, JsonObject o, List<Object> list) throws Exception {
        EmitterCollectorMessageType type = EmitterCollectorMessageType.ofJson(o);
        Log.debug("Decoding JsonObject for " + type.name());
        switch(type) {
            case EMITTER_HELLO:
                list.add(new EmitterHelloMessage(o));
                return;
            case RELAYED_LOG_PRINT:
                list.add(new RelayedLogPrintMessage(o));
                return;
            case RELAYED_STATF_PRINT:
                list.add(new RelayedStatFPrintMessage(o));
                return;
            case RELAYED_STATI_PRINT:
                list.add(new RelayedStatIPrintMessage(o));
                return;
            case PRINT_ACK:
                list.add(new PrintAckMessage(o));
                return;
            case RELAYED_CANDLESTICK_F_PRINT:
                list.add(new RelayedCandlestickFPrintMessage(o));
                return;
            case RELAYED_CANDLESTICK_I_PRINT:
                list.add(new RelayedCandlestickIPrintMessage(o));
                return;
            case RELAYED_STAT_ANNOUNCEMENT:
                list.add(new RelayedStatAnnouncementMessage(o));
                return;
            case EMITTER_HEARTBEAT:
                list.add(new EmitterHeartbeatMessage(o));
                return;
            default:
                throw new UnsupportedOperationException("Unhandled enum value: " + type.name());
        }
    }
}
