package cafe.josh.constellar.collector.model;

import cafe.josh.constellar.Received;
import cafe.josh.constellar.message.emittercollector.RelayedCandlestickFPrintMessage;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class CandlestickStatFPrint extends StatPrint {
    private final ZonedDateTime openTs, closeTs;
    private final double open, high, low, close;

    public CandlestickStatFPrint(ZonedDateTime mintTs, ZonedDateTime rcvTs, String statName, String host, ZonedDateTime openTs, ZonedDateTime closeTs, double open, double high, double low, double close) {
        super(mintTs, rcvTs, statName, host);
        this.openTs = openTs;
        this.closeTs = closeTs;
        this.open = open;
        this.high = high;
        this.low = low;
        this.close = close;
    }

    public CandlestickStatFPrint(ResultSet rs) throws SQLException {
        super(rs);
        this.openTs = rs.getObject("open_ts", OffsetDateTime.class).atZoneSameInstant(ZoneId.systemDefault());
        this.closeTs = rs.getObject("close_ts", OffsetDateTime.class).atZoneSameInstant(ZoneId.systemDefault());
        this.open = rs.getDouble("open");
        this.high = rs.getDouble("high");
        this.low = rs.getDouble("low");
        this.close = rs.getDouble("close");
    }

    public CandlestickStatFPrint(Received<RelayedCandlestickFPrintMessage> m) {
        this(m.getReceived().getMintTs(),
                m.getRcvTs(),
                m.getReceived().getStatName(),
                m.getHost(),
                m.getReceived().getOpenTs(),
                m.getReceived().getCloseTs(),
                m.getReceived().getOpen(),
                m.getReceived().getHigh(),
                m.getReceived().getLow(),
                m.getReceived().getClose());
    }

    @Override
    protected void valueToPreparedStatement(PreparedStatement ps) throws SQLException {
        ps.setObject(5, openTs.toOffsetDateTime().withOffsetSameInstant(ZoneOffset.UTC));
        ps.setObject(6, closeTs.toOffsetDateTime().withOffsetSameInstant(ZoneOffset.UTC));
        ps.setDouble(7, open);
        ps.setDouble(8, high);
        ps.setDouble(9, low);
        ps.setDouble(10, close);
    }

    @Override
    public String getInsertStatement() {
        return "insert into candlestick_f_print (mint_ts, rcv_ts, stat_name, host, open_ts, close_ts, open, high, low, close) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    }

    public ZonedDateTime getOpenTs() {
        return openTs;
    }

    public ZonedDateTime getCloseTs() {
        return closeTs;
    }

    public double getOpen() {
        return open;
    }

    public double getHigh() {
        return high;
    }

    public double getLow() {
        return low;
    }

    public double getClose() {
        return close;
    }

    public String getJSFormattedOpenTs() {
        return openTs.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME);
    }

    @Override
    public String toString() {
        return "CandlestickStatFPrint{" +
                "openTs=" + openTs +
                ", closeTs=" + closeTs +
                ", open=" + open +
                ", high=" + high +
                ", low=" + low +
                ", close=" + close +
                '}';
    }
}
