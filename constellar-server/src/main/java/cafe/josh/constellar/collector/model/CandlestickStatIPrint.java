package cafe.josh.constellar.collector.model;

import cafe.josh.constellar.Received;
import cafe.josh.constellar.message.emittercollector.RelayedCandlestickIPrintMessage;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class CandlestickStatIPrint extends StatPrint {
    private final ZonedDateTime openTs, closeTs;
    private final long open, high, low, close;

    public CandlestickStatIPrint(ZonedDateTime mintTs, ZonedDateTime rcvTs, String statName, String host, ZonedDateTime openTs, ZonedDateTime closeTs, long open, long high, long low, long close) {
        super(mintTs, rcvTs, statName, host);
        this.openTs = openTs;
        this.closeTs = closeTs;
        this.open = open;
        this.high = high;
        this.low = low;
        this.close = close;
    }

    public CandlestickStatIPrint(Received<RelayedCandlestickIPrintMessage> m) {
        this(m.getReceived().getMintTs(),
                m.getRcvTs(),
                m.getReceived().getStatName(),
                m.getHost(),
                m.getReceived().getOpenTs(),
                m.getReceived().getCloseTs(),
                m.getReceived().getOpen(),
                m.getReceived().getHigh(),
                m.getReceived().getLow(),
                m.getReceived().getClose());
    }

    public CandlestickStatIPrint(ResultSet rs) throws SQLException {
        super(rs);
        this.openTs = rs.getObject("open_ts", OffsetDateTime.class).atZoneSameInstant(ZoneId.systemDefault());
        this.closeTs = rs.getObject("close_ts", OffsetDateTime.class).atZoneSameInstant(ZoneId.systemDefault());
        this.open = rs.getLong("open");
        this.high = rs.getLong("high");
        this.low = rs.getLong("low");
        this.close = rs.getLong("close");
    }

    @Override
    protected void valueToPreparedStatement(PreparedStatement ps) throws SQLException {
        ps.setObject(5, openTs.toOffsetDateTime().withOffsetSameInstant(ZoneOffset.UTC));
        ps.setObject(6, closeTs.toOffsetDateTime().withOffsetSameInstant(ZoneOffset.UTC));
        ps.setLong(7, open);
        ps.setLong(8, high);
        ps.setLong(9, low);
        ps.setLong(10, close);
    }

    @Override
    public String getInsertStatement() {
        return "insert into candlestick_i_print (mint_ts, rcv_ts, stat_name, host, open_ts, close_ts, open, high, low, close) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    }

    public ZonedDateTime getOpenTs() {
        return openTs;
    }

    public ZonedDateTime getCloseTs() {
        return closeTs;
    }

    public long getOpen() {
        return open;
    }

    public long getHigh() {
        return high;
    }

    public long getLow() {
        return low;
    }

    public long getClose() {
        return close;
    }

    public String getJSFormattedOpenTs() {
        return openTs.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME);
    }

    @Override
    public String toString() {
        return "CandlestickStatIPrint{" +
                "openTs=" + openTs +
                ", closeTs=" + closeTs +
                ", open=" + open +
                ", high=" + high +
                ", low=" + low +
                ", close=" + close +
                '}';
    }
}
