package cafe.josh.constellar;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Log {
    private Log() {}

    public static void enableDebug() {
        Logger.getLogger("cafe.josh.constellar").setLevel(Level.FINE);
    }

    public static void info(String msg) {
        Logger.getLogger("cafe.josh.constellar").info(msg);
    }

    public static void debug(String msg) {
        Logger.getLogger("cafe.josh.constellar").fine(msg);
    }

    public static void warning(String msg) {
        Logger.getLogger("cafe.josh.constellar").warning(msg);
    }

    public static void warning(String msg, Throwable e) {
        Logger.getLogger("cafe.josh.constellar").log(Level.WARNING, msg, e);
    }

    public static void severe(String msg) {
        Logger.getLogger("cafe.josh.constellar").severe(msg);
    }

    public static void exception(String msg, Throwable e) {
        Logger.getLogger("cafe.josh.constellar").log(Level.SEVERE, msg, e);
    }

    public static void raw(String msg) {
        System.err.println(msg);
    }
}
