package cafe.josh.constellar.message;

import cafe.josh.constellar.message.util.MacAddress;
import org.junit.Test;
import static org.junit.Assert.*;

public class MacAddressTest {
    @Test
    public void testMacAddress() {
        assertEquals("DE:AD:BE:EF:FA:CE", new MacAddress("DE:AD:BE:EF:FA:CE").toString());
    }
}
