package cafe.josh.constellar.message;

import cafe.josh.constellar.Received;
import cafe.josh.constellar.collector.model.LogPrint;
import cafe.josh.constellar.message.clientemitter.LogLevel;
import cafe.josh.constellar.message.clientemitter.LogPrintMessage;
import cafe.josh.constellar.message.emittercollector.PrintAckMessage;
import cafe.josh.constellar.message.emittercollector.RelayedLogPrintMessage;
import com.google.gson.JsonParser;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.time.ZonedDateTime;
import java.util.UUID;

import static org.junit.Assert.*;

/**
 *
 * @author josh
 */
public class MessageTest {

    public MessageTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testLogPrintMessages() {
        JsonParser parser = new JsonParser();
        LogPrintMessage m = new LogPrintMessage("Test print", LogLevel.INFO, ZonedDateTime.now());
        String json = m.toString();
        LogPrintMessage m2 = new LogPrintMessage(parser.parse(json).getAsJsonObject());
        RelayedLogPrintMessage rm = new RelayedLogPrintMessage(m2, "TestService");
        json = rm.toString();
        RelayedLogPrintMessage rm2 = new RelayedLogPrintMessage(parser.parse(json).getAsJsonObject());
        final Received<RelayedLogPrintMessage> rrm = new Received<>(rm2, "TestHost");
        LogPrint modelObj = new LogPrint(rrm);

        assertEquals(m, m2);
        assertEquals(rm, rm2);

        assertEquals(rrm.getHost(), modelObj.getHost());
        assertEquals(rrm.getRcvTs(), modelObj.getRcvTs());
        assertEquals(rm.getServiceName(), modelObj.getServiceName());
        assertEquals(m.getLevel(), modelObj.getLevel());
        assertEquals(m.getMessage(), modelObj.getMessage());
        assertEquals(m.getMintTs(), modelObj.getMintTs());
    }

    @Test
    public void testAckMessage() {
        UUID uuid = UUID.randomUUID();
        PrintAckMessage msg = new PrintAckMessage(uuid);
        PrintAckMessage rebuilt = new PrintAckMessage(msg.toJson());
        assertEquals("Msg==Msg", msg.getAckedId(), rebuilt.getAckedId());
        assertEquals("UUID==rebuilt", uuid, rebuilt.getAckedId());
    }
}
