/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cafe.josh.constellar.message;

import cafe.josh.constellar.collectorui.RailsStyleRouter;
import cafe.josh.constellar.collectorui.RouteHandler;
import org.junit.*;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 *
 * @author josh
 */
public class RailsStyleRouterTest {

    public RailsStyleRouterTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testRootRoute() throws Exception {
        RailsStyleRouter router = new RailsStyleRouter(null);
        IGotCalledHandler callSlash = new IGotCalledHandler("get /");
        IGotCalledHandler notCalled1 = new IGotCalledHandler("get /health");
        IGotCalledHandler notCalled2 = new IGotCalledHandler("get /memes");
        router.register(callSlash);
        router.register(notCalled1);
        router.register(notCalled2);

        HttpServletRequest req = mock(HttpServletRequest.class);
        when(req.getPathInfo()).thenReturn(null);
        when(req.getMethod()).thenReturn("GET");
        HttpServletResponse resp = mock(HttpServletResponse.class);
        ServletContext cxt = mock(ServletContext.class);
        router.route(req, resp, cxt);

        assertTrue(callSlash.called);
        assertFalse(notCalled1.called);
        assertFalse(notCalled2.called);
    }

    @Test
    public void testPlainRoute() throws Exception {
        RailsStyleRouter router = new RailsStyleRouter(null);
        IGotCalledHandler called = new IGotCalledHandler("get /health");
        router.register(called);

        HttpServletRequest req = mock(HttpServletRequest.class);
        when(req.getPathInfo()).thenReturn("health");
        when(req.getMethod()).thenReturn("GET");
        HttpServletResponse resp = mock(HttpServletResponse.class);
        ServletContext cxt = mock(ServletContext.class);
        router.route(req, resp, cxt);

        assertTrue(called.called);
    }

    @Test
    public void testParameterizedRoute() throws Exception {
        RailsStyleRouter router = new RailsStyleRouter(null);
        IGotCalledHandler called = new IGotCalledHandler("get /statistic/:statName");
        router.register(called);

        HttpServletRequest req = mock(HttpServletRequest.class);
        when(req.getPathInfo()).thenReturn("statistic/meme_dankness_ratio");
        when(req.getMethod()).thenReturn("GET");
        HttpServletResponse resp = mock(HttpServletResponse.class);
        ServletContext cxt = mock(ServletContext.class);
        router.route(req, resp, cxt);

        assertTrue(called.called);
        assertNotNull(called.urlPartParams);
        assertEquals("meme_dankness_ratio", called.urlPartParams.get("statName"));
    }

    private static class IGotCalledHandler implements RouteHandler {
        public IGotCalledHandler(String route) {
            this.route = route;
        }
        private String route;
        public boolean called = false;
        public Map<String, String> urlPartParams = null;
        @Override
        public void handle(Map<String, String> urlPartParams, HttpServletRequest request, HttpServletResponse response, TemplateEngine templates, WebContext variables, ServletContext servletContext) throws Exception {
            this.called = true;
            this.urlPartParams = urlPartParams;
        }

        @Override
        public String getRoute() {
            return route;
        }
    }
}
