/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cafe.josh.constellar.message.gen;


import org.junit.*;
import static org.junit.Assert.assertEquals;

/**
 *
 * @author josh
 */
public class CaseConversionTest {

    public CaseConversionTest() {
    }

    @Test
    public void testCaseConversion() {
        assertEquals("one", MessageGenerator.camelToSnake("One"));
        assertEquals("one_two", MessageGenerator.camelToSnake("OneTwo"));
        assertEquals("just_a_variable", MessageGenerator.camelToSnake("justAVariable"));
        assertEquals("one", MessageGenerator.camelToSnake("one"));
    }

}
