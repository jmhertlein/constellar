package cafe.josh.constellar.message.clientemitter;

import java.util.logging.Level;

public enum LogLevel {
    DEBUG,
    INFO,
    WARNING,
    CRITICAL;

    public static LogLevel getFor(Level l) {
        if(l == Level.INFO) {
            return INFO;
        } else if(l == Level.SEVERE) {
            return CRITICAL;
        } else if(l == Level.WARNING) {
            return WARNING;
        } else {
            return DEBUG;
        }
    }
}
