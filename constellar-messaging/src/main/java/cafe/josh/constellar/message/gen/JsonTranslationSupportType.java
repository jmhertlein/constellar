package cafe.josh.constellar.message.gen;

public enum JsonTranslationSupportType {
    BUILTIN,
    ANNOTATED;
}
