package cafe.josh.constellar.message.clientemitter;

import com.google.gson.JsonObject;

public enum ClientEmitterMessageType {
    CLIENT_HELLO,
    LOG_PRINT,
    STATF_PRINT,
    STATI_PRINT,
    CANDLESTICK_I_PRINT,
    CANDLESTICK_F_PRINT,
    STAT_ANNOUNCEMENT,
    NONE;

    public static ClientEmitterMessageType ofJson(JsonObject o) {
        return ClientEmitterMessageType.valueOf(o.get("type").getAsString());
    }
}
