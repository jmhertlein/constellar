package cafe.josh.constellar.message.util;

import com.google.gson.Gson;

public class GsonSingleton {
    public static final Gson GSON = new Gson();
}
