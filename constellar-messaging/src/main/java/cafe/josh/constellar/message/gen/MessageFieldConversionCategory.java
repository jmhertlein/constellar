package cafe.josh.constellar.message.gen;

public enum MessageFieldConversionCategory {
    STRING(true, false),
    LONG(true, false),
    DOUBLE(true, false),
    BOOLEAN(true, false),
    LOCALDATETIME(true, false),
    DATETIME(true, false),
    ENUM(true, false),
    ANNOTATION(false, false),
    UUID(true, false),
    MAC_ADDRESS(true, false),
    INTEGER(true, false),
    LIST(true, true),
    MAP(true, true);

    private final boolean builtin, collection;

    MessageFieldConversionCategory(boolean builtin, boolean collection) {
        this.builtin = builtin;
        this.collection = collection;
    }

    public boolean isBuiltin() {
        return builtin;
    }

    public boolean isCollection() {
        return collection;
    }
}
