package cafe.josh.constellar.message.emittercollector;

import cafe.josh.constellar.message.util.GsonSingleton;
import com.google.gson.JsonObject;

import java.util.UUID;

public abstract class EmitterCollectorMessage {
    protected final transient UUID id;

    public EmitterCollectorMessage() {
        this.id = UUID.randomUUID();
    }

    public EmitterCollectorMessage(JsonObject o) {
        this.id = UUID.fromString(o.get("id").getAsString());
    }

    public UUID getId() {
        return id;
    }

    public abstract EmitterCollectorMessageType getMessageType();

    public JsonObject toJson() {
        JsonObject o = new JsonObject();
        o.addProperty("type", getMessageType().name());
        o.addProperty("id", id.toString());
        return o;
    }

    @Override
    public String toString() {
        return GsonSingleton.GSON.toJson(toJson());
    }
}
