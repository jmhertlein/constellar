package cafe.josh.constellar.message.gen;

import cafe.josh.constellar.message.clientemitter.ClientEmitterMessageType;
import cafe.josh.constellar.message.emittercollector.EmitterCollectorMessageType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.SOURCE)
@Target(ElementType.TYPE)
public @interface ProtoMessage {
    ClientEmitterMessageType ceType() default ClientEmitterMessageType.NONE;
    EmitterCollectorMessageType ecType() default EmitterCollectorMessageType.NONE;
    boolean isRelayed() default false;
}
