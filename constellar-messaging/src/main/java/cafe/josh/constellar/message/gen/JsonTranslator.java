package cafe.josh.constellar.message.gen;

import cafe.josh.constellar.message.util.MacAddress;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.squareup.javapoet.CodeBlock;
import com.squareup.javapoet.TypeName;

import javax.lang.model.element.ElementKind;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.PrimitiveType;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Elements;
import javax.lang.model.util.Types;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class JsonTranslator {
    public static MessageFieldConversionCategory categorize(Elements elements, Types types, TypeMirror t) throws CouldNotCategorizeTypeException {
        TypeName name = TypeName.get(t);

        if(t instanceof DeclaredType) {
            DeclaredType dc = (DeclaredType) t;
            if(TypeName.get(types.erasure(dc)).equals(TypeName.get(List.class))) {
                for (TypeMirror typeArgument : dc.getTypeArguments()) {
                    MessageFieldConversionCategory paramCategory = categorize(elements, types, typeArgument);
                    if (paramCategory == null || paramCategory.isCollection()) {
                        throw new CouldNotCategorizeTypeException(TypeName.get(dc), "Invalid type as type parameter.");
                    }
                }
                return MessageFieldConversionCategory.LIST;
            } else if(name.equals(TypeName.get(Map.class))) {
                for (TypeMirror typeArgument : dc.getTypeArguments()) {
                    MessageFieldConversionCategory paramCategory = categorize(elements, types, typeArgument);
                    if (paramCategory == null || paramCategory.isCollection()) {
                        throw new CouldNotCategorizeTypeException(TypeName.get(dc), "Invalid type as type parameter.");
                    }
                }
                return MessageFieldConversionCategory.MAP;
            } else if(name.equals(TypeName.get(LocalDateTime.class))) {
                return MessageFieldConversionCategory.LOCALDATETIME;
            } else if(name.equals(TypeName.get(ZonedDateTime.class))) {
                return MessageFieldConversionCategory.DATETIME;
            } else if(name.equals(TypeName.get(String.class))) {
                return MessageFieldConversionCategory.STRING;
            } else if(name.equals(TypeName.get(UUID.class))) {
                return MessageFieldConversionCategory.UUID;
            } else if(types.asElement(t).getKind() == ElementKind.ENUM) {
                return MessageFieldConversionCategory.ENUM;
            } else if(name.equals(TypeName.get(MacAddress.class))) {
                return MessageFieldConversionCategory.MAC_ADDRESS;
            } else if(t.getAnnotation(JsonTranslatable.class) != null) {
                return MessageFieldConversionCategory.ANNOTATION;
            }
        } else if(t instanceof PrimitiveType) {
            if(name.equals(TypeName.get(long.class))) {
                return MessageFieldConversionCategory.LONG;
            } else if(name.equals(TypeName.get(double.class))) {
                return MessageFieldConversionCategory.DOUBLE;
            } else if(name.equals(TypeName.get(boolean.class))) {
                return MessageFieldConversionCategory.BOOLEAN;
            } else if(name.equals(TypeName.get(int.class))) {
                return MessageFieldConversionCategory.INTEGER;
            }
        } else {
            throw new CouldNotCategorizeTypeException(TypeName.get(t), "Type was neither DeclaredType nor PrimitiveType");
        }

        throw new CouldNotCategorizeTypeException(TypeName.get(t), "Type not handled.");
    }



    public static CodeBlock listFromJsonBlock(String outputObjectName, CodeBlock jsonObjectExpr, TypeName genericType, MessageFieldConversionCategory genericCategory) {
        CodeBlock.Builder b = CodeBlock.builder();
        b.addStatement("$T<$T> $L = new $T<>();", List.class, genericType, outputObjectName, ArrayList.class);
        b.beginControlFlow("for($T jsonElement : ($L).getAsJsonArray())", JsonElement.class, jsonObjectExpr);
        b.addStatement("$L.add($L)", outputObjectName, getFromJsonExpression(CodeBlock.builder().add("jsonElement").build(), genericType, genericCategory));
        b.endControlFlow();
        return b.build();
    }

    public static CodeBlock listToJsonBlock(String jsonArrayObjectName, CodeBlock thingToAddToJsonExpr, TypeName genericType, MessageFieldConversionCategory genericCategory) {
        CodeBlock.Builder builder = CodeBlock.builder();
        builder.addStatement("$T $L = new $T()", JsonArray.class, jsonArrayObjectName, JsonArray.class);
        builder.beginControlFlow("for($T thingInList : $L)", genericType, thingToAddToJsonExpr);
        builder.addStatement("$L.add($L)", jsonArrayObjectName, getToJsonStringExpression(genericCategory, CodeBlock.of("thingInList")));
        builder.endControlFlow();

        return builder.build();
    }

    public static CodeBlock getFromJsonExpression(CodeBlock jsonObjectExpr, TypeName type, MessageFieldConversionCategory category) {
        CodeBlock.Builder builder = CodeBlock.builder();
        switch(category) {
            case ENUM:
                builder.add("$T.valueOf($L.getAsString())", type, jsonObjectExpr);
                break;
            case LOCALDATETIME:
                builder.add("$T.parse($L.getAsString(), $T.ISO_LOCAL_DATE_TIME)", LocalDateTime.class, jsonObjectExpr, DateTimeFormatter.class);
                break;
            case DATETIME:
                builder.add("$T.parse($L.getAsString(), $T.ISO_DATE_TIME)", ZonedDateTime.class, jsonObjectExpr, DateTimeFormatter.class);
                break;
            case STRING:
                builder.add("$L.getAsString()", jsonObjectExpr);
                break;
            case DOUBLE:
                builder.add("$L.getAsDouble()", jsonObjectExpr);
                break;
            case LONG:
                builder.add("$L.getAsLong()", jsonObjectExpr);
                break;
            case UUID:
                builder.add("UUID.fromString($L.getAsString())", jsonObjectExpr);
                break;
            case BOOLEAN:
                builder.add("$L.getAsBoolean()", jsonObjectExpr);
                break;
            case MAC_ADDRESS:
                builder.add("new $T($L.getAsString())", MacAddress.class, jsonObjectExpr);
                break;
            case INTEGER:
                builder.add("$L.getAsInt()", jsonObjectExpr);
                break;
            case ANNOTATION:
                builder.add("new $T($L)", type, jsonObjectExpr);
                break;
        }

        return builder.build();
    }

    public static CodeBlock getToJsonStringExpression(MessageFieldConversionCategory category, CodeBlock objectExpr) {
        CodeBlock.Builder builder = CodeBlock.builder();
        switch(category) {
            case ENUM:
                builder.add("$L.name()", objectExpr);
                break;
            case LOCALDATETIME:
                builder.add("$L.format($T.ISO_LOCAL_DATE_TIME)",  objectExpr, DateTimeFormatter.class);
                break;
            case DATETIME:
                builder.add("$L.format($T.ISO_DATE_TIME)",  objectExpr, DateTimeFormatter.class);
                break;
            case STRING:
                builder.add("$L",  objectExpr);
                break;
            case DOUBLE:
                builder.add("Double.toString($L)",  objectExpr);
                break;
            case LONG:
                builder.add("Long.toString($L)",  objectExpr);
                break;
            case BOOLEAN:
                builder.add("Boolean.toString($L)",  objectExpr);
                break;
            case UUID:
                builder.add("$L.toString()",  objectExpr);
                break;
            case MAC_ADDRESS:
                builder.add("$L.toString()",  objectExpr);
                break;
            case INTEGER:
                builder.add("Integer.toString($L)",  objectExpr);
                break;
            case ANNOTATION:
                builder.add("$L.serializeToJson()", objectExpr);
                break;
        }
        return builder.build();
    }

    public static class CouldNotCategorizeTypeException extends Exception {
        public CouldNotCategorizeTypeException(TypeName type, String msg) {
            super("Could not categorize type " + type + ": " + msg);
        }
    }
}
