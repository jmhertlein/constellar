package cafe.josh.constellar.message.gen;

import cafe.josh.constellar.message.clientemitter.ClientEmitterMessageType;
import cafe.josh.constellar.message.emittercollector.EmitterCollectorMessageType;

import javax.annotation.processing.Completion;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.Processor;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.*;
import javax.tools.Diagnostic;
import java.io.IOException;
import java.util.*;

public class MessageProcessor implements Processor {
    private ProcessingEnvironment env;
    public MessageProcessor() {

    }

    @Override
    public Set<String> getSupportedOptions() {
        return Collections.emptySet();
    }

    @Override
    public Set<String> getSupportedAnnotationTypes() {
        Set<String> supported = new HashSet<>();
        supported.add("cafe.josh.constellar.message.gen.ProtoMessage");
        supported.add("cafe.josh.constellar.message.gen.ProtoEmitterCollectorMessage");
        return supported;
    }

    @Override
    public SourceVersion getSupportedSourceVersion() {
        return SourceVersion.RELEASE_8;
    }

    @Override
    public void init(ProcessingEnvironment processingEnv) {
        this.env = processingEnv;
    }

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        annotations.forEach(annotation -> {
            roundEnv.getElementsAnnotatedWith(annotation)
                    .stream()
                    .filter(e -> e instanceof TypeElement)
                    .map(e -> (TypeElement) e)
                    .forEach(protoClass -> {
                        env.getMessager().printMessage(Diagnostic.Kind.NOTE, "Processing class: " + protoClass.getSimpleName());
                        ProtoMessage protoInfo = protoClass.getAnnotation(ProtoMessage.class);

                        if(annotation.getQualifiedName().equals(env.getElementUtils().getName(ProtoMessage.class.getCanonicalName())) && protoInfo != null) {
                            try {
                                if(protoInfo.ceType() != ClientEmitterMessageType.NONE) {
                                    MessageGenerator.generateClientEmitterMessage(protoInfo, protoClass, env).writeTo(env.getFiler());
                                }

                                if(protoInfo.ecType() != EmitterCollectorMessageType.NONE) {
                                    MessageGenerator.generateEmitterCollectorMessage(protoInfo, protoClass, env).writeTo(env.getFiler());
                                }
                            } catch (CodeGenerationException | IOException e) {
                                env.getMessager().printMessage(Diagnostic.Kind.ERROR, "Error generating code for class " + protoClass.getQualifiedName().toString());
                            }
                        }
                    });

        });

        return true;
    }

    @Override
    public Iterable<? extends Completion> getCompletions(Element element, AnnotationMirror annotation, ExecutableElement member, String userText) {
        return null;
    }
}
