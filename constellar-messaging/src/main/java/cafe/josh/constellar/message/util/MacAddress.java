package cafe.josh.constellar.message.util;

import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

public class MacAddress {
    private final byte[] bytes;

    public MacAddress(byte[] bytes) {
        if(bytes.length != 6) {
            throw new IllegalArgumentException("Where did you get a mac address that's " + bytes.length + " bytes long?");
        }
        this.bytes = bytes;
    }

    public MacAddress(String macAddress) {
        this.bytes = new byte[6];
        String[] octets = macAddress.split(":");
        if(octets.length != 6) {
            throw new IllegalArgumentException("Bad number of octets: " + octets.length);
        }
        for(int i = 0; i < octets.length; i++) {
            bytes[i] = (byte) Integer.parseUnsignedInt(octets[i], 16);
        }
    }

    public MacAddress(NetworkInterface networkInterface) throws SocketException {
        this.bytes = networkInterface.getHardwareAddress();
    }

    @Override
    public String toString() {
        List<String> octets = new ArrayList<>();
        for(int octetIndex = 0; octetIndex < bytes.length; octetIndex += 1) {
            String padded = String.format("%02x", bytes[octetIndex]);
            octets.add(padded);
        }

        return String.join(":", octets).toUpperCase();
    }

    public static MacAddress getMacAddress() throws SocketException, UnableToDetermineMacAddressException {
        List<byte[]> hwAddrs = new ArrayList<>();
        Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
        while (networkInterfaces.hasMoreElements()) {
            NetworkInterface networkInterface = networkInterfaces.nextElement();
            byte[] hwAddr = networkInterface.getHardwareAddress();
            if (hwAddr == null) {
                continue;
            }

            if (!networkInterface.getInetAddresses().hasMoreElements()) {
                continue;
            }

            hwAddrs.add(hwAddr);
        }

        if (hwAddrs.isEmpty()) {
            throw new UnableToDetermineMacAddressException("Unable to determine mac address.");
        }

        if (hwAddrs.size() > 1) {
            throw new UnableToDetermineMacAddressException("Too many eligible network interfaces to determine mac address.");
        }

        return new MacAddress(hwAddrs.get(0));
    }

    public static class UnableToDetermineMacAddressException extends Exception {
        public UnableToDetermineMacAddressException() {
        }

        public UnableToDetermineMacAddressException(String message) {
            super(message);
        }

        public UnableToDetermineMacAddressException(String message, Throwable cause) {
            super(message, cause);
        }

        public UnableToDetermineMacAddressException(Throwable cause) {
            super(cause);
        }

        public UnableToDetermineMacAddressException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
            super(message, cause, enableSuppression, writableStackTrace);
        }
    }
}
