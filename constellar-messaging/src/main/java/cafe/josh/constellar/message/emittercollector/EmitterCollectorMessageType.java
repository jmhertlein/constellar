package cafe.josh.constellar.message.emittercollector;

import com.google.gson.JsonObject;

public enum EmitterCollectorMessageType {
    EMITTER_HELLO(false),
    RELAYED_LOG_PRINT(true),
    RELAYED_STATF_PRINT(true),
    RELAYED_STATI_PRINT(true),
    PRINT_ACK(false),
    RELAYED_CANDLESTICK_I_PRINT(true),
    RELAYED_CANDLESTICK_F_PRINT(true),
    RELAYED_STAT_ANNOUNCEMENT(false),
    EMITTER_HEARTBEAT(false),
    NONE(false);

    private final boolean ackable;

    EmitterCollectorMessageType(boolean ackable) {
        this.ackable = ackable;
    }

    public boolean isAckable() {
        return ackable;
    }

    public static EmitterCollectorMessageType ofJson(JsonObject o) {
        return EmitterCollectorMessageType.valueOf(o.get("type").getAsString());
    }
}
