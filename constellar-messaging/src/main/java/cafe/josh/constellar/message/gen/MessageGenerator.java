package cafe.josh.constellar.message.gen;

import cafe.josh.constellar.message.clientemitter.ClientEmitterMessage;
import cafe.josh.constellar.message.emittercollector.EmitterCollectorMessage;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.squareup.javapoet.*;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.TypeMirror;
import javax.tools.Diagnostic;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static cafe.josh.constellar.message.gen.JsonTranslator.categorize;
import static cafe.josh.constellar.message.gen.JsonTranslator.getToJsonStringExpression;

public class MessageGenerator {
    private MessageGenerator() {}

    public static JavaFile generateClientEmitterMessage(ProtoMessage protoInfo, TypeElement protoClass, ProcessingEnvironment env) throws CodeGenerationException {
        return generateFile(env, protoClass, protoInfo.ceType(), ClientEmitterMessage.class, "cafe.josh.constellar.message.clientemitter", false);
    }

    public static JavaFile generateEmitterCollectorMessage(ProtoMessage protoInfo, TypeElement protoClass, ProcessingEnvironment env) throws CodeGenerationException {
        return generateFile(env, protoClass, protoInfo.ecType(), EmitterCollectorMessage.class, "cafe.josh.constellar.message.emittercollector", protoInfo.isRelayed());
    }

    private static JavaFile generateFile(ProcessingEnvironment env, TypeElement protoClass, Enum<?> messageType, Class<?> superClass, String pkg, boolean isRelayed) throws CodeGenerationException {
        String name = protoClass.getSimpleName().toString();
        if(!name.startsWith("Proto")) {
            throw new CodeGenerationException("The class names of proto messages must start with \"Proto\".");
        }

        String fabbedName;
        if(!isRelayed) {
            fabbedName = name.substring("Proto".length());
        } else {
            fabbedName = "Relayed" + name.substring("Proto".length());
        }
        TypeSpec.Builder fabbedMessage = TypeSpec.classBuilder(fabbedName);
        fabbedMessage.superclass(superClass);
        fabbedMessage.addModifiers(Modifier.PUBLIC);

        List<MessageField> fields = protoClass.getEnclosedElements()
                .stream()
                .filter(e -> e instanceof VariableElement)
                .map(e -> (VariableElement) e)
                .filter(v -> isRelayed || v.getAnnotation(RelayedOnly.class) == null)
                .map(v -> messageFieldForVariableElement(env, v))
                .collect(Collectors.toList());

        for(MessageField field : fields) {
            //env.getMessager().printMessage(Diagnostic.Kind.NOTE, field.field.name + ": " + field.category.name());
            //System.out.println(field.field.name + ": " + field.category.name());
            fabbedMessage.addField(field.field);
            fabbedMessage.addMethod(field.getter);
        }

        fabbedMessage.addMethod(getMessageTypeMethod(messageType));

        MethodSpec.Builder toJsonMethodBuilder = MethodSpec.methodBuilder("toJson").returns(JsonObject.class)
                .addModifiers(Modifier.PUBLIC)
                .addStatement("$T o = super.toJson()", JsonObject.class);
        for (MessageField field : fields) {
            try {
                toJsonMethodBuilder.addCode(getAddToJsonBlock(env, field));
            } catch (JsonTranslator.CouldNotCategorizeTypeException e) {
                throw new RuntimeException(e);
            }
        }
        toJsonMethodBuilder.addStatement("return o");
        fabbedMessage.addMethod(toJsonMethodBuilder.build());

        MethodSpec.Builder normalCtor = MethodSpec.constructorBuilder();
        normalCtor.addModifiers(Modifier.PUBLIC);
        for(MessageField f : fields) {
            normalCtor.addParameter(f.field.type, f.field.name);
            normalCtor.addStatement("this.$L = $L", f.field.name, f.field.name);
        }
        fabbedMessage.addMethod(normalCtor.build());

        MethodSpec.Builder fromJsonCtor = MethodSpec.constructorBuilder();
        fromJsonCtor.addModifiers(Modifier.PUBLIC);
        fromJsonCtor.addParameter(JsonObject.class, "o");
        fromJsonCtor.addStatement("super(o)");
        for (MessageField field : fields) {
            CodeBlock jsonObjectExpr = CodeBlock.builder().add("o.get($S)", camelToSnake(field.field.name)).build();
            try {
                addFromJsonBlockForField(env, fromJsonCtor, field, jsonObjectExpr);
            } catch (JsonTranslator.CouldNotCategorizeTypeException e) {
                throw new RuntimeException(e);
            }

        }
        fabbedMessage.addMethod(fromJsonCtor.build());

        if(isRelayed) {
            MethodSpec.Builder relayedCtor = MethodSpec.constructorBuilder();
            relayedCtor.addModifiers(Modifier.PUBLIC);
            relayedCtor.addParameter(ClassName.get("cafe.josh.constellar.message.clientemitter", fabbedName.substring("Relayed".length())),
                    "msg");
            for (MessageField field : fields) {
                if(field.element.getAnnotation(RelayedOnly.class) != null) {
                    relayedCtor.addParameter(field.field.type, field.field.name);
                    relayedCtor.addStatement("this.$L = $L", field.field.name, field.field.name);
                } else {
                    relayedCtor.addStatement("this.$L = msg.$L()", field.field.name, field.getter.name);
                }
            }
            fabbedMessage.addMethod(relayedCtor.build());
        }

        MethodSpec.Builder equalsMethod = MethodSpec.methodBuilder("equals").addAnnotation(Override.class);
        equalsMethod.addModifiers(Modifier.PUBLIC);
        equalsMethod.addParameter(Object.class, "obj");
        equalsMethod.returns(boolean.class);
        equalsMethod.beginControlFlow("if(this == obj)")
                .addStatement("return true")
                .endControlFlow();
        equalsMethod.beginControlFlow("if(obj == null)")
                .addStatement("return false")
                .endControlFlow();
        equalsMethod.beginControlFlow("if(getClass() != obj.getClass())")
                .addStatement("return false")
                .endControlFlow();
        equalsMethod.addStatement("final $L other = ($L) obj", fabbedName, fabbedName);
        for(MessageField field : fields) {
            if(field.field.type.isPrimitive()) {
                equalsMethod.beginControlFlow("if($N != other.$L)", field.field, field.field.name)
                        .addStatement("return false")
                        .endControlFlow();
            } else {
                equalsMethod.beginControlFlow("if(!$T.equals($N, other.$L))", Objects.class, field.field, field.field.name)
                        .addStatement("return false")
                        .endControlFlow();
            }
        }
        equalsMethod.addStatement("return true");
        fabbedMessage.addMethod(equalsMethod.build());

        MethodSpec.Builder hashcodeMethod = MethodSpec.methodBuilder("hashCode");
        hashcodeMethod.addAnnotation(Override.class);
        hashcodeMethod.addModifiers(Modifier.PUBLIC);
        hashcodeMethod.returns(int.class);
        List<String> nodes = new ArrayList<>();
        for (MessageField ignore : fields) {
            nodes.add("$N");
        }
        Object[] more = new Object[fields.size()+1];
        more[0] = Objects.class;
        System.arraycopy(fields.stream().map(f -> f.field).toArray(), 0, more, 1, fields.size());
        hashcodeMethod.addStatement("return $T.hash(" + String.join(", ", nodes) + ")", more);
        fabbedMessage.addMethod(hashcodeMethod.build());

        JavaFile ret = JavaFile.builder(pkg, fabbedMessage.build()).build();
        env.getMessager().printMessage(Diagnostic.Kind.NOTE, "Generate: " + ret.typeSpec.name);
        return ret;
    }

    private static void addFromJsonBlockForField(ProcessingEnvironment env, MethodSpec.Builder fromJsonCtor, MessageField field, CodeBlock jsonObjectExpr) throws JsonTranslator.CouldNotCategorizeTypeException {
        if(field.category == MessageFieldConversionCategory.LIST) {
            String outputObjectName = field.field.name + "DeserializedList";
            DeclaredType dt = (DeclaredType) field.element.asType();
            TypeMirror argType = dt.getTypeArguments().get(0);
            MessageFieldConversionCategory argCategory = JsonTranslator.categorize(env.getElementUtils(), env.getTypeUtils(), argType);
            CodeBlock listFromJsonBlock = JsonTranslator.listFromJsonBlock(outputObjectName,
                    jsonObjectExpr,
                    TypeName.get(argType),
                    argCategory
            );
            fromJsonCtor.addCode(listFromJsonBlock);
            fromJsonCtor.addStatement("this.$L = $L", field.field.name, outputObjectName);
        } else if(field.category == MessageFieldConversionCategory.MAP) {

        } else {
            fromJsonCtor.addStatement("this.$L = $L", field.field.name, JsonTranslator.getFromJsonExpression(jsonObjectExpr, field.field.type, field.category));
        }
    }

    private static CodeBlock getAddToJsonBlock(ProcessingEnvironment env, MessageField field) throws JsonTranslator.CouldNotCategorizeTypeException {
        CodeBlock thingToAddToJsonExpr = CodeBlock.builder().add("$N", field.field).build();
        CodeBlock.Builder builder = CodeBlock.builder();
        if(field.category == MessageFieldConversionCategory.LIST) {
            DeclaredType dt = (DeclaredType) field.element.asType();
            TypeMirror argType = dt.getTypeArguments().get(0);
            MessageFieldConversionCategory argCategory = JsonTranslator.categorize(env.getElementUtils(), env.getTypeUtils(), argType);

            String jsonArrayObjectName = field.field.name + "JsonArray";
            builder.add(JsonTranslator.listToJsonBlock(jsonArrayObjectName, thingToAddToJsonExpr, TypeName.get(argType), argCategory));
            builder.addStatement("o.add($S, $L)", camelToSnake(field.field.name), jsonArrayObjectName);
        } else if(field.category == MessageFieldConversionCategory.MAP) {

        } else {
            CodeBlock toJsonStringExpr = getToJsonStringExpression(field.category, thingToAddToJsonExpr);
            builder.addStatement("o.addProperty($S, $L)", camelToSnake(field.field.name), toJsonStringExpr);
        }

        return builder.build();
    }

    private static MethodSpec getterFor(FieldSpec field) {
        String verb;
        if(field.type.equals(TypeName.BOOLEAN)) {
            verb = "is";
        } else {
            verb = "get";
        }

        String upperCamelCaseName = field.name.substring(0, 1).toUpperCase() + field.name.substring(1);

        return MethodSpec.methodBuilder(verb + upperCamelCaseName)
                .addModifiers(Modifier.PUBLIC)
                .returns(field.type)
                .addStatement("return $N", field)
                .build();
    }


    private static MethodSpec getMessageTypeMethod(Enum<?> type) {
        return MethodSpec.methodBuilder("getMessageType")
                .addModifiers(Modifier.PUBLIC)
                .returns(type.getDeclaringClass())
                .addAnnotation(Override.class)
                .addStatement("return $T.$L", type.getDeclaringClass(), type.name())
                .build();
    }

    private static MessageField messageFieldForVariableElement(ProcessingEnvironment env, VariableElement e) {
        MessageField field = new MessageField(e);
        field.field  = FieldSpec.builder(TypeName.get(field.element.asType()), field.element.getSimpleName().toString(), Modifier.PRIVATE, Modifier.FINAL).build();
        field.getter = getterFor(field.field);
        try {
            field.category = categorize(env.getElementUtils(), env.getTypeUtils(), field.element.asType());
        } catch (JsonTranslator.CouldNotCategorizeTypeException e1) {
            throw new RuntimeException(e1);
        }
        return field;
    }

    static String camelToSnake(String camel) {
        StringBuilder snake = new StringBuilder();
        char[] chars = camel.toCharArray();
        for(int i = 0; i < chars.length; i++) {
            if(i == 0) {
                snake.append(Character.toLowerCase(chars[i]));
            } else {
                if(Character.isUpperCase(chars[i])) {
                    snake.append('_').append(Character.toLowerCase(chars[i]));
                } else {
                    snake.append(chars[i]);
                }
            }
        }

        return snake.toString();
    }

    private static class MessageField {
        public VariableElement element;
        public MethodSpec getter;
        public FieldSpec field;
        public MessageFieldConversionCategory category;

        public MessageField(VariableElement e) {
            element = e;
        }
    }
}
