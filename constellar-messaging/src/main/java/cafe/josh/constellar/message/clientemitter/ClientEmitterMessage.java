package cafe.josh.constellar.message.clientemitter;

import cafe.josh.constellar.message.util.GsonSingleton;
import com.google.gson.JsonObject;

public abstract class ClientEmitterMessage {
    public ClientEmitterMessage() {

    }

    public ClientEmitterMessage(JsonObject o) {

    }

    public abstract ClientEmitterMessageType getMessageType();
    public JsonObject toJson() {
        JsonObject o = new JsonObject();
        o.addProperty("type", getMessageType().name());
        return o;
    }

    @Override
    public String toString() {
        return GsonSingleton.GSON.toJson(toJson());
    }
}
