#!/usr/bin/bash

cd ~/projects/constellar
psql -h octavian -d constellar algalon -f schema/drop.sql
psql -h octavian -d constellar algalon -f schema/setup.sql
