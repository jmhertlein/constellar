create type log_level as enum ('debug', 'info', 'warning', 'critical');
create type stat_type as enum ('float', 'int');
create type visualization_type as enum ('line_chart', 'candlestick_chart');

create table statistic (
  stat_name text primary key,
  pretty_name text not null,
  stat_type stat_type not null,
  viz visualization_type not null,
  host_specific boolean not null
);

create table host (
  hostname text primary key,
  mac macaddr not null,
  last_ip inet not null,
  last_seen timestamptz not null,
  cpu_count integer not null,
  mem_bytes bigint not null
);

create table log_print (
  mint_ts timestamptz not null,
  rcv_ts timestamptz not null,
  host text not null references host(hostname),
  service_name text not null,
  message text not null,
  level log_level not null
);

create table stati_print (
  mint_ts timestamptz not null,
  rcv_ts timestamptz not null,
  stat_name text references statistic(stat_name) not null,
  value bigint not null,
  host text not null references host(hostname)
);

create table statf_print (
  mint_ts timestamptz not null,
  rcv_ts timestamptz not null,
  stat_name text references statistic(stat_name) not null,
  value double precision not null,
  host text not null references host(hostname)
);

create table candlestick_f_print (
  mint_ts timestamptz not null,
  rcv_ts timestamptz not null,
  stat_name text references statistic(stat_name) not null,
  open_ts timestamptz not null,
  close_ts timestamptz not null,
  open double precision not null,
  high double precision not null,
  low double precision not null,
  close double precision not null,
  host text not null references host(hostname)
);

create table candlestick_i_print (
  mint_ts timestamptz not null,
  rcv_ts timestamptz not null,
  stat_name text references statistic(stat_name) not null,
  open_ts timestamptz not null,
  close_ts timestamptz not null,
  open bigint not null,
  high bigint not null,
  low bigint not null,
  close bigint not null,
  host text not null references host(hostname)
);

SELECT create_hypertable('log_print', 'mint_ts');
SELECT create_hypertable('stati_print', 'mint_ts');
SELECT create_hypertable('statf_print', 'mint_ts');
SELECT create_hypertable('candlestick_i_print', 'open_ts');
SELECT create_hypertable('candlestick_f_print', 'open_ts');
